import Vue from "vue";
import Vuex from "vuex";

import PostAddModule from "./modules/post_add.js";
import PostAcModule from "./modules/post_ac.js";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        post_add: PostAddModule,
        post_ac: PostAcModule,
    }
});
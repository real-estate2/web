import Router from "vue-router";

// POST
import PostAdd from "../views/post/add/index";

import Vue from "vue";
Vue.use(Router);

// Routes
const router = new Router({
    linkExactActiveClass: "active", // link active
    mode: "hash",
    base: process.env.BASE_URL,
    routes: [
        { path: '/', name: 'post.add', component: PostAdd },
    ]
});

export default router;
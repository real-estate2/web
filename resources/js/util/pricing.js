export const unitPriceList = [168.181, 68.181, 50, 27.272, 454.545, 1.727];
export const unitPriceAddonList = [250, 100, 75, 40, 680, 5];

export const priceLevel = [
  [
    {
      Name: 0,
      Value: "Thỏa thuận"
    },
    {
      Name: 1,
      Value: "< 500 triệu"
    },
    {
      Name: 2,
      Value: "500 - 800 triệu"
    },
    {
      Name: 3,
      Value: "800 triệu - 1 tỷ"
    },
    {
      Name: 4,
      Value: "1 - 2 tỷ"
    },
    {
      Name: 5,
      Value: "2 - 3 tỷ"
    },
    {
      Name: 6,
      Value: "3 - 5 tỷ"
    },
    {
      Name: 7,
      Value: "5 - 7 tỷ"
    },
    {
      Name: 8,
      Value: "7 - 10 tỷ"
    },
    {
      Name: 9,
      Value: "10 - 20 tỷ"
    },
    {
      Name: 10,
      Value: "20 - 30 tỷ"
    },
    {
      Name: 11,
      Value: "> 30 tỷ"
    }
  ],
  [
    {
      Name: 0,
      Value: "Thỏa thuận"
    },
    {
      Name: 1,
      Value: "< 1 triệu"
    },
    {
      Name: 2,
      Value: "1 - 3 triệu"
    },
    {
      Name: 3,
      Value: "3 - 5 triệu"
    },
    {
      Name: 4,
      Value: "5 - 10 triệu"
    },
    {
      Name: 5,
      Value: "10 - 40 triệu"
    },
    {
      Name: 6,
      Value: "40 - 70 triệu"
    },
    {
      Name: 7,
      Value: "70 - 100 triệu"
    },
    {
      Name: 8,
      Value: "> 100 triệu"
    }
  ]
];
export const priceLevelByType = [
  {
    data: [
      {
        Name: 0,
        Value: "Thỏa thuận"
      },
      {
        Name: 1,
        Value: "< 500 triệu"
      },
      {
        Name: 2,
        Value: "500 - 800 triệu"
      },
      {
        Name: 3,
        Value: "800 triệu - 1 tỷ"
      },
      {
        Name: 4,
        Value: "1 - 2 tỷ"
      },
      {
        Name: 5,
        Value: "2 - 3 tỷ"
      },
      {
        Name: 6,
        Value: "3 - 5 tỷ"
      },
      {
        Name: 7,
        Value: "5 - 7 tỷ"
      },
      {
        Name: 8,
        Value: "7 - 10 tỷ"
      },
      {
        Name: 9,
        Value: "10 - 20 tỷ"
      },
      {
        Name: 10,
        Value: "20 - 30 tỷ"
      },
      {
        Name: 11,
        Value: "> 30 tỷ"
      }
    ],
    code: 38
  },
  {
    data: [
      {
        Name: 0,
        Value: "Thỏa thuận"
      },
      {
        Name: 1,
        Value: "< 1 triệu"
      },
      {
        Name: 2,
        Value: "1 - 3 triệu"
      },
      {
        Name: 3,
        Value: "3 - 5 triệu"
      },
      {
        Name: 4,
        Value: "5 - 10 triệu"
      },
      {
        Name: 5,
        Value: "10 - 40 triệu"
      },
      {
        Name: 6,
        Value: "40 - 70 triệu"
      },
      {
        Name: 7,
        Value: "70 - 100 triệu"
      },
      {
        Name: 8,
        Value: "> 100 triệu"
      }
    ],
    code: 49
  }
];

export function getMoneyText(money) {
  if (money <= 0) {
    return "0 đồng";
  }

  money = Math.round(money * 10) / 10;
  let retval = "";
  let sodu = 0;

  if (money >= 1000000000) {
    sodu = Math.floor(money / 1000000000);
    retval += sodu + " tỷ ";
    money = money - sodu * 1000000000;
  }

  if (money >= 1000000) {
    sodu = Math.floor(money / 1000000);
    retval += sodu + " triệu ";
    money = money - sodu * 1000000;
  }

  if (money >= 1000) {
    sodu = Math.floor(money / 1000);
    retval += sodu + " nghìn ";
    money = money - sodu * 1000;
  }

  money = Math.floor(money);

  if (money > 0) {
    retval += money + " đồng";
  }

  return retval.trim();
}

export function getMoneyText2(money) {
  money = Math.round(money * 10) / 10;

  let sodu = 0;
  if (money >= 1000000000) {
    sodu = money / 1000000000;
    return sodu + " tỷ ";
  }
  if (money >= 1000000) {
    sodu = money / 1000000;
    return sodu + " triệu ";
  }

  return getMoneyText(money);
}

export function getPriceForPostType(postType) {
  return unitPriceList[postType];
}

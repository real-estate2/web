import axios from "axios";

/**
 * Tạo danh mục mới
 *
 * @param data
 * @returns {Promise<AxiosResponse<T>>}
 */

function submit(data) {
  return axios.post("api/post.customer/create", data);
}

export default {
  submit
};

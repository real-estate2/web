<div class="form-group">
    <div class="row">
        <div class="col col-sm-12">
            <label class="control-label">Bản đồ</label>
            <br />
            Để tăng độ tin cậy và tin rao được nhiều người quan tâm hơn, hãy sửa vị
            trí tin rao của bạn trên bản đồ bằng cách Chọn lại vị trí bản đồ
        </div>
        <div class="col col-sm-9">
            <div id="mapid" onchange="onMapClick"></div>
        </div>
        <div class="col col-sm-3">
            <label class="control-label"><b>Kinh độ</b></label>
            <div class="longitude">
                <input disabled="true" name="longitude" id="longitude" class="form-control" placeholder="Bạn chưa chọn kinh độ" type="text"/>
            </div>
            <br>
            <label class="control-label"><b>Vĩ độ</b></label>
            <div class="latitude">
                <input disabled="true" name="latitude" id="latitude" class="form-control" placeholder="Bạn chưa chọn vĩ độ" type="text"/>
            </div>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col col-sm-2">
            <label class="control-label">Hướng nhà</label>
            <select name="main_direction" class="form-control select2" style="width: 100%;">
                <option value="0">Không xác định</option>
                <option value="1">Chánh Đông</option>
                <option value="2">Chánh Tây</option>
                <option value="3">Chánh Nam</option>
                <option value="4">Chánh Bắc</option>
                <option value="5">Đông-Bắc</option>
                <option value="6">Tây-Bắc</option>
                <option value="7">Tây-Nam</option>
                <option value="8">Đông-Nam</option>
            </select>
        </div>

        <div class="col col-sm-2">
            <label class="control-label">Số phòng ngủ</label>
            <input name="nbr_bed_room" class="form-control" placeholder="ví dụ: 1" type="text"/>
        </div>

        <div class="col col-sm-2">
            <label class="control-label">Số Toilet</label>
            <input name="nbr_rest_room" class="form-control" placeholder="ví dụ: 1" type="text"/>
        </div>

        <div class="col col-sm-2">
            <label class="control-label">Số tầng</label>
            <input name="nbr_floor" class="form-control" placeholder="ví dụ: 1" type="text"/>
        </div>
        <div class="col col-sm-2">
            <label class="control-label">Mặt tiền</label>
            <input name="front_street_width" class="form-control" placeholder="ví dụ: 1" type="text"/>
        </div>
        <div class="col col-sm-2">
            <label class="control-label">Đường vào</label>
            <input name="behind_street_width" class="form-control" placeholder="ví dụ: 1" type="text"/>
        </div>
    </div>
</div>
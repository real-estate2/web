<div class="form-group col-sm-12">
    <div class="row">
        <label class="control-label">Hình ảnh</label>
        <br/>
        Tối đa 8 ảnh với tin thường và 16 với tin VIP. Mỗi ảnh tối đa 5MB<br/>
        Tin rao có ảnh sẽ được xem nhiều hơn gấp 10 lần và được nhiều người gọi
        gấp 5 lần so với tin rao không có ảnh. Hãy đăng ảnh để được giao dịch
        nhanh chóng!<br/>

        <br/>
        Đăng ảnh thật nhanh bằng cách kéo và thả ảnh vào khung. Thay đổi vị trí
        của ảnh bằng cách kéo ảnh vào vị trí mà bạn muốn!
        <div class="col col-sm-12 dm-uploader">
            <label for="images" class="custom-file-upload">
                <i class="fa fa-cloud-upload"></i> Chọn từ máy tính
            </label>
            <input class="show-for-sr" id="images" name="images[]" accept="image/*" multiple=""
                   title="Bấm vào đây để chọn file" type="file"/>
            <input class="update btn btn-success" type="button" value="Xác nhận hình"/>
        </div>
        <br/>
        <div id="image_preview" name="image_preview"></div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col col-sm-12">
            <label class="control-label">Video</label>
            <div class="youtube-link">
                <input onchange="youtube" name="youtube" id="youtube" class="form-control youtube"
                       placeholder="Vd: https://www.youtube.com/watch?v=nHK0u40Ompc" type="text"/>
            </div>
            <br>
            <p id="showVideo"></p>
        </div>
    </div>
</div>
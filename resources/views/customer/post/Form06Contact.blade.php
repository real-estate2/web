<div class="form-group">
    <div class="row">
        <div class="col col-sm-3">
            <label class="control-label">Họ tên liên hệ</label>
            <input name="contact_name" class="form-control" placeholder="Họ tên..." type="text"/>
        </div>

        <div class="col col-sm-3">
            <label class="control-label">Địa chỉ liên hệ</label>
            <input name="contact_address" class="form-control" placeholder="Địa chỉ..." type="text"/>
        </div>

        <div class="col col-sm-3">
            <label class="control-label">Điện thoại liên hệ</label>
            <input name="contact_mobile" class="form-control" placeholder="Số điện thoại..." type="text"/>
        </div>

        <div class="col col-sm-3">
            <label class="control-label">Email liên hệ</label>
            <input name="contact_email" class="form-control" placeholder="Email..." type="text"/>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col col-sm-12">
            <label class="control-label">Tiêu đề</label>
            <input name="title" class="form-control" placeholder="Tiêu đề..." type="text"/>
        </div>
    </div>
</div>
<div class="form-group">
    <div class="row">
        <div class="col col-sm-3">
            <label class="control-label">Hình thức</label>
            <select name="product_type" id="product_type" class="form-control select2" style="width: 100%;">
                <option value="0">Chọn hình thức</option>
                <option value="38">Nhà đất bán</option>
                <option value="49">Nhà đất cho thuê</option>
            </select>
        </div>
        <div class="col col-sm-3">
            <label class="control-label">Loại</label>
            <select name="product_category" id="product_category" class="form-control select2" style="width: 100%;">
                <option class="form-control" value="">Chọn loại hình</option>
            </select>
        </div>
        <div class="col col-sm-3">
            <label class="control-label">Giá</label>
            <input name="price" class="form-control" placeholder="5, 10, 15,.." type="text"/>
        </div>

        <div class="col col-sm-3">
            <label class="control-label">Đơn vị giá</label>
            <select name="price_unit" id="price_unit" class="form-control select2" style="width: 100%;">
                <option class="form-control" value="">Chọn đơn vị giá</option>
            </select>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col col-sm-4">
            <label class="control-label">Dự án</label>
            <select name="project_id" class="form-control select2" style="width: 100%;">
                <option class="form-control" value="0">Không</option>
            </select>
        </div>
        <div class="col col-sm-4">
            <label class="control-label">Diện tích</label>
            <input name="area" class="form-control" placeholder="Tổng diện tích..." type="text"/>
        </div>

        <div class="col col-sm-4">
            <label class="control-label">Đơn vị diện tích</label>
            <input name="area_unit" class="form-control" value="m2" placeholder="m2" type="text"/>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col col-sm-3">
            <label class="control-label">Tỉnh/Thành phố</label>
            <select name="province" id="province" class="form-control select2" style="width: 100%;">
                @foreach ($province as $info)
                    <option value="{{ $info['province_id'] }}">{{ $info['name'] }}</option>
                @endforeach
            </select>
        </div>

        <div class="col col-sm-3">
            <label class="control-label">Quận/Huyện</label>
            <select name="district" id="district" class="form-control select2" style="width: 100%;">
                <option class="form-control" value="">Chọn quận/huyện</option>
            </select>
        </div>

        <div class="col col-sm-3">
            <label class="control-label">Phường/Xã</label>
            <select name="ward" id="ward" class="form-control select2" style="width: 100%;">
                <option class="form-control" value="">Chọn phường/xã</option>
            </select>
        </div>
        <div class="col col-sm-3">
            <label class="control-label">Đường/Hẻm</label>
            <select name="street" id="street" class="form-control select2" style="width: 100%;">
                <option class="form-control" value="">Chọn đường/hẻm</option>
            </select>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="row">
        <div class="col col-sm-12">
            <label class="control-label">Địa chỉ</label>
            <input name="address" class="form-control" placeholder="Địa chỉ của dự án..." type="text"/>
        </div>
    </div>
</div>
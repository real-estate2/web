@extends('layouts.master')
@section('css_head')
    @parent
    <link rel="stylesheet" href="https://d19vzq90twjlae.cloudfront.net/leaflet-0.7.3/leaflet.css"/>
    <style>
        .dm-uploader {
            border: 0.25rem dashed #a5a5c7;
            min-height: 100px;
        }

        .item {
            position: relative;
            padding-top: 20px;
            display: inline-block;
        }

        .notify-badge {
            position: absolute;
            right: 1rem;
            top: 1rem;
            background: red;
            text-align: center;
            border-radius: 5px 5px 5px 5px;
            color: white;
            padding: 5px 5px;
            font-size: 20px;
        }

        /*// Custom button*/
        input[type="file"] {
            display: none;
        }

        .custom-file-upload {
            /*display: inline-block;*/
            padding: 6px 12px;
            cursor: pointer;

            display: table;
            margin: 20px auto;
            border: 1px solid #ccc;
        }

        .update {
            padding: 6px 12px;
            cursor: pointer;

            display: table;
            margin: 20px auto;
            border: 1px solid #ccc;
        }

        .img-preview-thumb {
            background: #fff;
            border: 1px solid #777;
            border-radius: 0.25rem;
            box-shadow: 0.125rem 0.125rem 0.0625rem rgba(0, 0, 0, 0.12);
            margin-right: 1rem;
            max-width: 140px;
            padding: 0.25rem;
        }

        /*=============== BUTTON REMOVE IMG*/
        .remove {
            display: block;
            background: #444;
            border: 1px solid black;
            color: white;
            text-align: center;
            cursor: pointer;
        }

        .remove:hover {
            background: white;
            color: black;
        }

        #mapid {
            width: auto;
            height: 500px;
            border: 0;
            z-index: 0;
        }
    </style>
@endsection
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <h5>ĐĂNG TIN CỦA BẠN</h5>
                    </nav>
                    <hr>
                    <h6>Khu cho thuê bật nhất trong khu vực.</h6>
                    <div class="row">

                    </div>
                    <div class="text-center mt-3">
                    </div>
                </div>
            </div>
            <form action="{{route('form.post')}}" method="POST" enctype="multipart/form-data">
                @include('customer.post.Form01BasicInfo')
                {{--            CKeditor chưa đc responsive--}}
                @include('customer.post.Form02Description')
                @include('customer.post.Form03OtherInfo')
                @include('customer.post.Form04PictureAndVideo')
                @include('customer.post.Form05Map')
                @include('customer.post.Form06Contact')
                @include('customer.post.Form07Schedule')
                <div class="footer">
                    <button type="submit" class="btn btn-success">Đăng</button>
                    <button class="btn btn-danger">Làm mới</button>
                </div>
                {{csrf_field()}}
            </form>
        </div>
    </section>
@endsection
@section('js_footer')
    @parent
    {{--    ============================================    XỬ LÝ PROVINCE - DISTRICT   ==========================================--}}
    <script type="text/javascript">
        $(document).ready(function () {
            var cateList = [
                {
                    id: 38,
                    name: "Nhà đất bán",
                    children: [
                        {value: "0", text: "--- Chọn danh mục bán ---"},
                        {value: "324", text: "Bán căn hộ chung cư"},
                        {value: "41", text: "Bán nhà riêng"},
                        {value: "325", text: "Bán nhà biệt thự, liền kề"},
                        {value: "163", text: "Bán nhà mặt phố"},
                        {value: "40", text: "Bán đất nền dự án"},
                        {value: "283", text: "Bán đất"},
                        {value: "44", text: "Bán trang trại, khu nghỉ dưỡng"},
                        {value: "45", text: "Bán kho, nhà xưởng"},
                        {value: "48", text: "Bán loại bất động sản khác"}
                    ],
                    prices: [
                        {value: "0", text: "Thỏa thuận"},
                        {value: "1", text: "Triệu"},
                        {value: "2", text: "Tỷ"},
                        {value: "6", text: "Trăm nghìn/m2"},
                        {value: "7", text: "Triệu/m2"}
                    ]
                },
                {
                    id: 49,
                    name: "Nhà đất cho thuê",
                    children: [
                        {value: "0", text: "--- Chọn danh mục cho thuê ---"},
                        {value: "326", text: "Cho thuê căn hộ chung cư"},
                        {value: "52", text: "Cho thuê nhà riêng"},
                        {value: "51", text: "Cho thuê nhà mặt phố"},
                        {value: "57", text: "Cho thuê nhà trọ, phòng trọ"},
                        {value: "50", text: "Cho thuê văn phòng"},
                        {value: "55", text: "Cho thuê cửa hàng, ki ốt"},
                        {value: "53", text: "Cho thuê kho, nhà xưởng, đất"},
                        {value: "59", text: "Cho thuê loại bất động sản khác"}
                    ],
                    prices: [
                        {value: "0", text: "Thỏa thuận"},
                        {value: "1", text: "Trăm nghìn/tháng"},
                        {value: "2", text: "Triệu/tháng"},
                        {value: "5", text: "Trăm nghìn/m2/tháng"},
                        {value: "6", text: "Triệu/m2/tháng"},
                        {value: "7", text: "Nghìn/m2/tháng"}
                    ]
                }
            ];

            $('select[name="product_type"]').on('change', function () {
                let productTypeID = $(this).val();

                $('select[name="product_category"]').empty();
                $('select[name="price_unit"]').empty();
                $.each(cateList, function (key, value) {
                    if (value.id != productTypeID) {
                        console.log('no');
                        return true;
                    }
                    $.each(value.children, function (key, value) {
                        $('select[name="product_category"]').append('<option value="' + value.value + '">' + value.text + '</option>');
                    });
                    $.each(value.prices, function (key, value) {
                        $('select[name="price_unit"]').append('<option value="' + value.value + '">' + value.text + '</option>');
                    });
                });
            });
            // ==================================   PROVINCE - DISTRICT -WARD - STREET ============================
            $('select[name="province"]').on('change', function () {
                var provinceID = $(this).val();
                if (provinceID) {
                    $.ajax({
                        url: '/province.getDistricts/' + encodeURI(provinceID),
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $('select[name="district"]').empty();
                            $.each(data, function (key, value) {
                                $('select[name="district"]').append('<option value="' + value.district_id + '">' + value.pre + ' ' + value.name + '</option>');
                            });
                        }
                    });
                } else {
                    $('select[name="district"]').empty();
                }
            });
            $('select[name="district"]').on('change', function () {
                var districtID = $(this).val();
                if (districtID) {
                    $.ajax({
                        url: '/district.getWards/' + encodeURI(districtID),
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $('select[name="ward"]').empty();
                            $.each(data, function (key, value) {
                                $('select[name="ward"]').append('<option value="' + value.ward_id + '">' + value.name + '</option>');
                            });
                        }
                    });
                    $.ajax({
                        url: '/district.getStreets/' + encodeURI(districtID),
                        type: "GET",
                        dataType: "json",
                        success: function (data) {
                            $('select[name="street"]').empty();
                            $.each(data, function (key, value) {
                                $('select[name="street"]').append('<option value="' + value.street_id + '">' + value.name + '</option>');
                            });
                        }
                    });
                } else {
                    $('select[name="ward"]').empty();
                }
            });
        });
    </script>
    {{--    ============================================    XỬ LÝ CKEDITOR   ==========================================--}}
    <script src="https://cdn.ckeditor.com/ckeditor5/17.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>

    {{--    ===================================================     XỬ LÝ IMAGE     ===========================================--}}
    <script type="text/javascript">
        ////////////////////////
        $("#images").on("change", function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            let formData = new FormData();
            let files = e.target.files;
            for (let i = 0; i < files.length; i++) {
                let file = files[i];
                formData.append("file", file);
                $.ajax({
                    url: 'image.upload',
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData,
                    success: function (result) {
                        var mediaInfo = {
                            files: [],
                            preview: [],
                            thumbnails: "",
                        };

                        mediaInfo.files.push(result.data);
                        for (let i = 0; i < result.data["files"].length; i++) {
                            if (result.data["files"][i]["ranting"] === "xs") {
                                thumbnailUrl = "http://127.0.0.1:8085/images/post/" + result.data["files"][i]["filename"];
                                mediaInfo.thumbnails = "http://127.0.0.1:8085/images/post/" + result.data["files"][i]["filename"];
                            }
                            if (result.data["files"][i]["ranting"] === "o") {
                                rawUrl = "http://127.0.0.1:8085/images/post/" + result.data["files"][i]["filename"];
                            }
                        }

                        // console.log(mediaInfo.files);
                        $('#image_preview').append('<input hidden name="thumbnails" id="thumbnails" value="'+thumbnailUrl+'" type="text"/>');


                        mediaInfo.preview.push({
                            type: result.data.type,
                            thumbnail: thumbnailUrl,
                            raw: rawUrl
                        });

                        for (let i = 0; i < mediaInfo.preview.length; i++) {
                            $("<span class=\"pip\">" +
                                "<img width='100px' class=\"imageThumb\" src=\"" +mediaInfo.preview[i].thumbnail+ "\"/>" +
                                "<br/><span class=\"remove\">Xóa</span>" +
                                "</span>").insertAfter("#image_preview");
                            $(".remove").click(function () {
                                $(this).parent(".pip").remove();
                                mediaInfo.files.splice(i, 1);
                                mediaInfo.preview.splice(i, 1);
                                $('#images').val("");

                                console.log('========>');
                            });
                        }
                    }
                });
            }
        });

        //////////////////////////

        // $(function () {
        //     $("#images").on("change", function (e) {
        //         // Stops the form from reloading
        //         e.preventDefault();
        //         $.ajaxSetup({
        //             headers: {
        //                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //             }
        //         });
        //         let image_upload = new FormData();
        //
        //         let TotalImages = $('#images')[0].files.length;  //Total Images
        //         let images = $('#images')[0];
        //
        //         for (let i = 0; i < TotalImages; i++) {
        //             $("<span class=\"pip\">" +
        //                 "<img width='100px' class=\"imageThumb\" src=\"" +URL.createObjectURL(images.files[i])+ "\"/>" +
        //                 "<br/><span class=\"remove\">Xóa</span>" +
        //                 "</span>").insertAfter("#image_preview");
        //             $(".remove").click(function(){
        //                 $(this).parent(".pip").remove();
        //                 $('#images').val("");
        //             });
        //
        //             image_upload.append('images' + i, images.files[i]);
        //         }
        //         image_upload.append('TotalImages', TotalImages);
        //
        //         $(".update").click(function (e) {
        //             // e.preventDefault();
        //             $.ajax({
        //                 url: 'image.upload',
        //                 type: 'POST',
        //                 contentType: false,
        //                 processData: false,
        //                 data: image_upload,
        //                 success: function (result) {
        //                     // alert(result.message);
        //                     // console.log(result.arrimg);
        //                     $('#image_preview').append('<input hidden name="thumbnails" id="thumbnails" value="http://127.0.0.1:8085/images/post/'+result.filename+'" type="text"/>');
        //                 }
        //             });
        //         });
        //     });
        // });
    </script>
    {{--    ================================================    XỬ LÝ YOUTUBE    ========================================--}}
    <script>
        $('input[name=youtube]').change(function () {
            yt_url = null;
            yt_id = null;
            $('#showVideo').empty();
            $(this).each(function () {
                var yt_url = this.value,
                    yt_id = yt_url.split('?v=')[1];
                if (yt_id != null) {
                    $('input[name=youtube]').append('<input onchange="youtube" name="youtube" id="youtube" value="http://www.youtube.com/embed/' + yt_id + '" class="form-control youtube" type="text"/>');
                    $('#showVideo').append('<iframe height="400px" width="100%" class="youtube-frame" src="http://www.youtube.com/embed/' + yt_id + '"></iframe>');
                } else {
                    $('#showVideo').append('<p id="showVideo"><b class="text-danger">XIN LỖI ĐƯỜNG DẪN CỦA BẠN KHÔNG PHÙ HỢP</b></p>');
                }
            });
        });
    </script>
    {{--    =============================================    XỬ LÝ BẢN ĐỒ   =======================================--}}
    <script src="https://d19vzq90twjlae.cloudfront.net/leaflet-0.7.3/leaflet.js"></script>
    <script>
        let mymap = L.map('mapid').setView([10.9804, 106.6519], 14);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1
        }).addTo(mymap);

        let theMarker = {};

        function onMapClick(e) {
            $('.longitude').empty();
            $('.latitude').empty();
            lat = e.latlng.lat;
            lng = e.latlng.lng;
            $('.longitude').append('<input type="text" name="longitude" id="longitude" class="form-control" value="' + lng + '"/>');
            $('.latitude').append('<input type="text" name="latitude" id="latitude" class="form-control" value="' + lat + '"/>');
            popup
                .setLatLng(e.latlng)
                .setContent("Tọa độ bạn chọn là: " + e.latlng.toString())
                .openOn(mymap);

            if (theMarker !== undefined) {
                mymap.removeLayer(theMarker);
            }

            theMarker = L.marker([lat, lng]).addTo(mymap);
        }

        mymap.on('click', onMapClick);

        let popup = L.popup();
    </script>
@endsection
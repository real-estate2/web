<div class="form-group">
    <div class="row">
        <div class="col col-sm-4">
            <label class="control-label">Loại tin rao</label>
            <select name="post_type" class="form-control select2" style="width: 100%;">
                <option value="0">Tin thường</option>
                <option value="1">Vip 1</option>
                <option value="2">Vip 2</option>
                <option value="3">Vip 3</option>
            </select>
        </div>

        <div class="col col-sm-4">
            <label class="control-label">Ngày bắt đầu</label>
            <input name="start_date" class="form-control" type="date" />
        </div>

        <div class="col col-sm-4">
            <label class="control-label">Ngày kết thúc</label>
            <input name="end_date" class="form-control" type="date" />
        </div>
    </div>
</div>
{{--<div class="col-sm-6">--}}
{{--      Đơn giá cuối cùng: <span class="text-primary text-md"><b> pricing.per_day | moneyString }}--}}
{{--            <span v-if="form.post_type !== 4">/Ngày</span></b></span>--}}
{{--</div>--}}
{{--<div class="col-sm-6">--}}
{{--    Số ngày:--}}
{{--    <span class="text-primary text-md"--}}
{{--    ><b>scheduleInfo.numberOfDay }} ngày</b></span--}}
{{--    >--}}
{{--</div>--}}
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Young Genaration - Dịch vụ bất động sản</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="/logo/logo.ico" type="image/icon type">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<base href="{{asset('')}}">

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Muli" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
	<link rel="stylesheet" href="assets/style.css">
	<link rel="stylesheet" href="assets/toast/toastr.min.css">
	<link rel="stylesheet" href="assets/selectize.default.css" data-theme="default">
	<link rel="stylesheet" href="assets/fileinput.css">
	<link rel="stylesheet" href="assets/pgwslider/pgwslider.min.css">
	<link rel="stylesheet" href="css/main.css">
	<style>
		.top-right {
		position: absolute;
		top: 8px;
		right: 16px;
		color: red;
		}
		.crop-text {
            /* width: 220px;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis; */

			height: 100px;
			overflow: hidden;
			text-overflow: ellipsis;
			white-space: pre-line;
        }
	</style>
	@yield('css_head')
</head>
<body>

    @include('layouts.menu')
    @yield('content')
	@include('layouts.common.chatbox')
	@include('layouts.common.croll')
	<div class="gap"></div>
	@include('layouts.footer')


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/toast/toastr.min.js"></script>
    @yield('js_footer')
	{{--CROLL TOP--}}
	<script>
		window.$ = window.jQuery = $;

		$('' +
				'#cuon').click(function () {
			$('body,html').animate({scrollTop: 0}, 600);
			return false;
		});

		$(window).scroll(function () {
			if ($(window).scrollTop() === 0) {
				$('#cuon').stop(false, true).fadeOut(300);
			} else {
				$('#cuon').stop(false, true).fadeIn(300);
			}
		});
	</script>
</body>
</html>

<footer class="mainfooter" role="contentinfo">
    <div class="footer-middle">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <!--Column1-->
                    <div class="footer-pad">
                        <img width="250" style="margin-top: -10px;" src="/logo/logoknl.png" alt="Kỷ nguyên land">
                        <ul class="list-unstyled">
                            <li>Là nơi chúng tôi đã lựa chọn rất kỹ càng và cho ra những bất động sản tiềm năng nhất cho tất cả mọi người cùng đầu tư và phát triển.</li>
                            <br>
                            <li>Địa chỉ: 06, Trần Văn Ơn, phường Phú Hòa, TP.Thủ Dầu Một, tỉnh Bình Dương</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 pl-5">
                    <!--Column1-->
                    <div class="footer-pad">
                        <h4>YG</h4>
                        <ul class="list-unstyled">
                            <li>
                                <a href="{{route('contact')}}">Về chúng tôi</a>
                            </li>
                            <li>
                                <a href="#">Đối tác</a>
                            </li>
                            <li><a href="#">Chính sách bảo mật</a></li>
                            <li><a href="#">Quy chế hoạt động</a></li>
                            <li><a href="#">Điều khoản hoạt động</a></li>
                            <li><a href="#">Tin Tức</a></li>
                            <li><a href="#">Liên Hệ</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <!--Column1-->
                    <div class="footer-pad">
                        <h4>HỖ TRỢ</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">Câu hỏi thường gặp</a></li>
                            <li><a href="#">Hướng dẫn sử dụng</a></li>
                            <li><a href="#">Quyền lợi thành viên</a></li>
                            <li><a href="#">Hotline 0936.929.237</a></li>
                            <li><a href="#">Chăm sóc khách hàng cskh@yg.vn</a></li>
                            <li>
                                <a href="#"></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <h4>KẾT NỐI</h4>
                    <ul class="social-network social-circle">
                        <li><a href="#" class="icoFacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 copy">
                    <p class="text-center">&copy; Copyright 2020 - © Young Genaration</p>
                </div>
            </div>


        </div>
    </div>
</footer>
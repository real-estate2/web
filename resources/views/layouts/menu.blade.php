{{--<nav class="navbar navbar-expand-lg navbar-dark bg-dark">--}}
<nav class="navbar navbar-expand-lg" style="background: white;">
    <div class="container">
        <a class="navbar-brand" href="/">
            <img class="img-responsive" width="150px" src="/logo/logoknl.png" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" style="color: #D2691E" href="{{route('sell.index')}}">Bán</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="color: #D2691E" href="{{route('rent.index')}}">Cho thuê</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="color: #D2691E" href="{{route('project.category')}}">Dự án</a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" style="color: #D2691E" href="{{route('news.index')}}">Tin tức</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="color: #D2691E" href="{{route('agency.index')}}">Về chúng tôi</a>
                </li>

            </ul>

            @if(Auth::check())
                <a href="{{route('post.form')}}" class="btn btn-outline-warning my-2 my-sm-0 ml-3">Đăng tin ngay</a>
                <a href="#"
                   class="btn btn-outline-info my-2 my-sm-0 ml-3">{{Auth::user()->username}}</a>
                <a href="{{route('logout.get')}}" class="btn btn-danger my-2 my-sm-0 ml-3">Đăng xuất</a>
            @else
                <a href="{{route('login.get')}}" style="color: #D2691E" class="btn btn-outline-warning my-2 my-sm-0 ml-3">Đăng
                    Nhập</a>
            @endif
        </div>
    </div>
</nav>
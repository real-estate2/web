@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-sm-12 col-sx-12 mt-3">
                <h2>{{$item->title}}</h2>
                <div class="mt-2 mb-2">
                    <span><i class="fas fa-stopwatch"></i> {{$item->created_at}}</span>
                </div>
                <img src="http://127.0.0.1:8000/images/news/{{ $item->thumbnails }}" class="img-thumbnail">
                {!! $item->content !!}

                Nguồn tin: <a target="_blank" href="{{$item->source}}">{{$item->source}}</a>

            </div>
            <div class="col-lg-4 mt-3" id="utilities">
                {{--                Tin liên quan--}}
                @include('news.common.similar-news')
            </div>
        </div>
    </div>
@endsection

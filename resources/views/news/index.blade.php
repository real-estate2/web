@extends('layouts.master')

@section('content')
    <section id="layerbackground">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12 mt-2">
                    <span>TIN TỨC NỔI BẬT</span>
                    <hr>
                    <h6>Chúng tôi mang đến cho bạn đầy đủ thông tin cập nhật mọi lúc mọi nơi</h6>
                    <div class="row">
                        @foreach($news as $item)
                            <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12 mt-3">
                                <div id="" class="card">
                                    <a href="{{route('news.detail', ['slug' => $item->slug, 'id' => $item->id])}}" class="" style="text-decoration: none">
                                        <img src="http://127.0.0.1:8000/images/news/{{$item->thumbnails}}"
                                             class="card-img-top">
                                        <div class="card-body">
                                            <h5 class="card-title crop-text" style="text-transform:uppercase">{{$item->title}}</h5>
                                            <p>{{$item->getNewsCategoryString}}</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center mt-3">
{{--                        <a href="danh-sac" id="btn-xemtatca" class="btn btn-primary">Xem thêm</a>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

<div class="alert alert-dark" role="alert">
    <h4>Tin liên quan</h4>
</div>
@foreach($similar_data as $item)
    <a href="{{route('news.detail', ['slug' => $item->slug, 'id' => $item->id])}}" style="text-decoration: none">
        <div class="row media mt-1" style="background: white; color: #1d2124">
            <div class="col-md-5">
                <img class="img-fluid"
                     src="http://127.0.0.1:8000/images/news/{{$item->thumbnails}}"
                     alt="Generic placeholder image">
            </div>
            <div class="media-body col-md-7">
                <p class="mt-0">{{$item->title}}</p>
            </div>
        </div>
    </a>
@endforeach
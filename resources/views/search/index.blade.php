@extends('layouts.master')

@section('content')
{{--        <div class="container">--}}
{{--            @foreach ($posts as $order)--}}
{{--                {{ $order->title }}--}}
{{--            @endforeach--}}
{{--        </div>--}}


    <section id="layerbackground">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        @if(count($posts) > 0)
                            <h5>Tìm thấy {{count($posts)}} sản phẩm</h5>
                        @else
                            <h5 class="text-danger">Không có sản phẩm nào phù hợp!</h5>
                        @endif
                    </nav>
{{--                    @if(count($project) > 0)--}}
{{--                        <hr>--}}
{{--                        <h6>Dự án bật nhất trong khu vực.</h6>--}}
{{--                        <div class="row">--}}
{{--                            @foreach($project as $item)--}}
{{--                                <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12 mt-3">--}}
{{--                                    <div class="card">--}}
{{--                                        <a href="{{route('project.details', ['slug' => $item->slug, 'id' => $item->id])}}"--}}
{{--                                           data-toggle="tooltip" data-placement="right" title="{{$item->title}}"--}}
{{--                                           class="" style="text-decoration: none">--}}
{{--                                            <img src="{{$item->thumbnails}}"--}}
{{--                                                 height="200"--}}
{{--                                                 class="card-img-top">--}}
{{--                                            <div class="card-body">--}}
{{--                                                <h6 class="card-title text-info crop-text"--}}
{{--                                                    style="text-transform:uppercase">--}}
{{--                                                    {{$item->title}}--}}
{{--                                                </h6>--}}
{{--                                                <div class="card-subtitle mb-2 text-muted">--}}
{{--                                                    <i class="fas fa-map-marked-alt"></i>--}}
{{--                                                    <span style="float: right;">Bình dương</span>--}}

{{--                                                </div>--}}
{{--                                                <div class="card-subtitle mb-2 text-muted">--}}
{{--                                                    <i class="far fa-money-bill-alt"></i>--}}
{{--                                                    <span style="float: right;" class="text-danger">--}}
{{--                                                        {{ number_format($item->price, 2) }} {{$item->getProductPriceUnitString}}--}}
{{--                                                    </span>--}}
{{--                                                </div>--}}
{{--                                                <div class="card-subtitle mb-2 text-muted">--}}
{{--                                                    <i class="fas fa-expand"></i>--}}
{{--                                                    <span style="float: right;">{{$item->area}} m<sup>2</sup></span>--}}

{{--                                                </div>--}}
{{--                                                <div class="card-subtitle mb-2 text-muted">--}}
{{--                                                    <i class="fas fa-th-large"></i>--}}
{{--                                                    <span style="float: right;">{{$item->getProductCategoryString}}</span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    @endif--}}

{{--                    @if(count($sell) > 0)--}}
{{--                        <hr>--}}
{{--                        <h6>Khu bán bật nhất trong khu vực.</h6>--}}
{{--                        <div class="row">--}}
{{--                            @foreach($sell as $item)--}}
{{--                                <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12 mt-3">--}}
{{--                                    <div class="card">--}}
{{--                                        <a href="{{route('sell.details', ['slug' => $item->slug, 'id' => $item->id])}}"--}}
{{--                                           data-toggle="tooltip" data-placement="right" title="{{$item->title}}"--}}
{{--                                           class="" style="text-decoration: none">--}}
{{--                                            <img src="{{$item->thumbnails}}"--}}
{{--                                                 height="200"--}}
{{--                                                 class="card-img-top">--}}
{{--                                            <div class="card-body">--}}
{{--                                                <h6 class="card-title text-info crop-text"--}}
{{--                                                    style="text-transform:uppercase">--}}
{{--                                                    {{$item->title}}--}}
{{--                                                </h6>--}}
{{--                                                <div class="card-subtitle mb-2 text-muted">--}}
{{--                                                    <i class="fas fa-map-marked-alt"></i>--}}
{{--                                                    <span style="float: right;">Bình dương</span>--}}

{{--                                                </div>--}}
{{--                                                <div class="card-subtitle mb-2 text-muted">--}}
{{--                                                    <i class="far fa-money-bill-alt"></i>--}}
{{--                                                    <span style="float: right;" class="text-danger">--}}
{{--                                                        {{ number_format($item->price, 2) }} {{$item->getProductPriceUnitString}}--}}
{{--                                                    </span>--}}
{{--                                                </div>--}}
{{--                                                <div class="card-subtitle mb-2 text-muted">--}}
{{--                                                    <i class="fas fa-expand"></i>--}}
{{--                                                    <span style="float: right;">{{$item->area}} m<sup>2</sup></span>--}}

{{--                                                </div>--}}
{{--                                                <div class="card-subtitle mb-2 text-muted">--}}
{{--                                                    <i class="fas fa-th-large"></i>--}}
{{--                                                    <span style="float: right;">{{$item->getProductCategoryString}}</span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}
{{--                        </div>--}}
{{--                    @endif--}}

                    @if(count($posts) > 0)
                        <hr>
                        <h6>Tìm kiếm dễ dàng mọi thứ trong khu vực.</h6>
                        <div class="row">
                            @foreach($posts as $item)
                                <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12 mt-3">
                                    <div class="card">
                                        <a href="{{route('rent.details', ['slug' => $item->slug, 'id' => $item->id])}}"
                                           data-toggle="tooltip" data-placement="right" title="{{$item->title}}"
                                           class="" style="text-decoration: none">
                                            <img src="{{$item->thumbnails}}"
                                                 height="200"
                                                 class="card-img-top">
                                            <div class="card-body">
                                                <h6 class="card-title text-info crop-text"
                                                    style="text-transform:uppercase">
                                                    {{$item->title}}
                                                </h6>
                                                <div class="card-subtitle mb-2 text-muted">
                                                    <i class="fas fa-map-marked-alt"></i>
                                                    <span style="float: right;">Bình dương</span>

                                                </div>
                                                <div class="card-subtitle mb-2 text-muted">
                                                    <i class="far fa-money-bill-alt"></i>
                                                    <span style="float: right;" class="text-danger">
                                                        {{ number_format($item->price, 2) }} {{$item->getProductPriceUnitString}}
                                                    </span>
                                                </div>
                                                <div class="card-subtitle mb-2 text-muted">
                                                    <i class="fas fa-expand"></i>
                                                    <span style="float: right;">{{$item->area}} m<sup>2</sup></span>

                                                </div>
                                                <div class="card-subtitle mb-2 text-muted">
                                                    <i class="fas fa-th-large"></i>
                                                    <span style="float: right;">{{$item->getProductCategoryString}}</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                    <div class="text-center mt-3">
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

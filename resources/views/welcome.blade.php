<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>

{{--    // Thêm vào sau--}}
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Muli" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    <link rel="stylesheet" href="assets/style.css">
    <link rel="stylesheet" href="assets/toast/toastr.min.css">
    <link rel="stylesheet" href="assets/selectize.default.css" data-theme="default">
    <link rel="stylesheet" href="assets/fileinput.css">
    <link rel="stylesheet" href="assets/pgwslider/pgwslider.min.css">
    <style>
        #cuon {
            display: block;
            position: fixed;
            bottom: 20px;
            right: 15px;
        }
        /*// DÙng cho footer*/
        footer {
            background: #1D2C4C;
            color: white;
            font-family: 'Muli' ,sans-serif;
        }
        footer a {
            color: #fff;
            font-size: 14px;
            transition-duration: 0.2s;
        }

        footer a:hover {
            color: #FA944B;
            text-decoration: none;
        }

        .copy {
            font-size: 12px;
            padding: 10px;
            border-top: 1px solid #FFFFFF;
        }

        .footer-middle {
            padding-top: 2em;
            color: white;
        }


        /*SOCİAL İCONS*/

        /* footer social icons */

        ul.social-network {
            list-style: none;
            display: inline;
            margin-left: 0 !important;
            padding: 0;
        }

        ul.social-network li {
            display: inline;
            margin: 0 5px;
        }


        /* footer social icons */

        .social-network a.icoFacebook:hover {
            background-color: #3B5998;
        }

        .social-network a.icoLinkedin:hover {
            background-color: #007bb7;
        }

        .social-network a.icoFacebook:hover i,
        .social-network a.icoLinkedin:hover i {
            color: #fff;
        }

        .social-network a.socialIcon:hover,
        .socialHoverClass {
            color: #44BCDD;
        }

        .social-circle li a {
            display: inline-block;
            position: relative;
            margin: 0 auto 0 auto;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
            text-align: center;
            width: 30px;
            height: 30px;
            font-size: 15px;
        }

        .social-circle li i {
            margin: 0;
            line-height: 30px;
            text-align: center;
        }

        .social-circle li a:hover i,
        .triggeredHover {
            -moz-transform: rotate(360deg);
            -webkit-transform: rotate(360deg);
            -ms--transform: rotate(360deg);
            transform: rotate(360deg);
            -webkit-transition: all 0.2s;
            -moz-transition: all 0.2s;
            -o-transition: all 0.2s;
            -ms-transition: all 0.2s;
            transition: all 0.2s;
        }

        .social-circle i {
            color: #595959;
            -webkit-transition: all 0.8s;
            -moz-transition: all 0.8s;
            -o-transition: all 0.8s;
            -ms-transition: all 0.8s;
            transition: all 0.8s;
        }

        .social-network a {
            background-color: #F9F9F9;
        }
        .list-unstyled>li {
            padding: 3px;
        }

        /*// GIỚI HẠN TEXT TITLE*/
        .crop-text {
            max-height: 4.5em;
            overflow: hidden;
            position: relative;
            line-height: 1.5em;
            display:inline-block;
        }

        .crop-text::before {
            background: linear-gradient(to right, rgba(255, 255, 255, 0) 0%, white 50%);
            content: '\200C';
            display: block;
            position: absolute;
            right: 0;
            top: 3em;
            width:20%;
            text-align: center;
        }
    </style>
    <style>
        #phone-home {
            padding: 5px;
            border-radius: 20px;
            display: block;
            position: fixed;
            bottom: 15px;
            left: 25px;
            text-decoration: none;
        }
        .Phone {
            position: relative;
            display: block;
            margin: 0;
            width: 1em;
            height: 1em;
            font-size: 10vmin;
            background-color: #DA8028;
            border-radius: 0.5em;
        }
        .Phone::before, .Phone::after {
            position: absolute;
            content: "";
        }
        .Phone::before {
            top: 0;
            left: 0;
            width: 1em;
            height: 1em;
            background-color: rgba(255, 255, 255, 0.1);
            border-radius: 100%;
            opacity: 1;
            transform: translate3d(0, 0, 0) scale(0);
        }
        .Phone::after {
            top: 0.25em;
            left: 0.25em;
            width: 0.5em;
            height: 0.5em;
            background: url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNCAyNCI+PHBhdGggZD0iTTYuNiAxMC44YzEuNCAyLjggMy44IDUuMSA2LjYgNi42bDIuMi0yLjJjLjMtLjMuNy0uNCAxLS4yIDEuMS40IDIuMy42IDMuNi42LjUgMCAxIC40IDEgMVYyMGMwIC41LS41IDEtMSAxLTkuNCAwLTE3LTcuNi0xNy0xNyAwLS42LjQtMSAxLTFoMy41Yy41IDAgMSAuNCAxIDEgMCAxLjIuMiAyLjUuNiAzLjYuMS40IDAgLjctLjIgMWwtMi4zIDIuMnoiIGZpbGw9IiNmZmZmZmYiLz48L3N2Zz4=);
            background-position: 50% 50%;
            background-repeat: no-repeat;
            background-size: cover;
            transform: translate3d(0, 0, 0);
        }
        .Phone.is-animating {
            animation: phone-outer 3000ms infinite;
        }
        .Phone.is-animating::before {
            animation: phone-inner 3000ms infinite;
        }
        .Phone.is-animating::after {
            animation: phone-icon 3000ms infinite;
        }
        @keyframes phone-outer {
            0% {
                transform: translate3d(0, 0, 0) scale(1);
                box-shadow: 0 0 0 0em rgba(52, 152, 219, 0), 0em 0.05em 0.1em rgba(0, 0, 0, 0.2);
            }
            33.3333% {
                transform: translate3d(0, 0, 0) scale(1.1);
                box-shadow: 0 0 0 0em rgba(52, 152, 219, 0.1), 0em 0.05em 0.1em rgba(0, 0, 0, 0.5);
            }
            66.6666% {
                transform: translate3d(0, 0, 0) scale(1);
                box-shadow: 0 0 0 0.5em rgba(52, 152, 219, 0), 0em 0.05em 0.1em rgba(0, 0, 0, 0.2);
            }
            100% {
                transform: translate3d(0, 0, 0) scale(1);
                box-shadow: 0 0 0 0em rgba(52, 152, 219, 0), 0em 0.05em 0.1em rgba(0, 0, 0, 0.2);
            }
        }
        @keyframes phone-inner {
            0% {
                opacity: 1;
                transform: translate3d(0, 0, 0) scale(0);
            }
            33.3333% {
                opacity: 1;
                transform: translate3d(0, 0, 0) scale(0.9);
            }
            66.6666% {
                opacity: 0;
                transform: translate3d(0, 0, 0) scale(0);
            }
            100% {
                opacity: 0;
                transform: translate3d(0, 0, 0) scale(0);
            }
        }
        @keyframes phone-icon {
            0% {
                transform: translate3d(0em, 0, 0);
            }
            2% {
                transform: translate3d(0.01em, 0, 0);
            }
            4% {
                transform: translate3d(-0.01em, 0, 0);
            }
            6% {
                transform: translate3d(0.01em, 0, 0);
            }
            8% {
                transform: translate3d(-0.01em, 0, 0);
            }
            10% {
                transform: translate3d(0.01em, 0, 0);
            }
            12% {
                transform: translate3d(-0.01em, 0, 0);
            }
            14% {
                transform: translate3d(0.01em, 0, 0);
            }
            16% {
                transform: translate3d(-0.01em, 0, 0);
            }
            18% {
                transform: translate3d(0.01em, 0, 0);
            }
            20% {
                transform: translate3d(-0.01em, 0, 0);
            }
            22% {
                transform: translate3d(0.01em, 0, 0);
            }
            24% {
                transform: translate3d(-0.01em, 0, 0);
            }
            26% {
                transform: translate3d(0.01em, 0, 0);
            }
            28% {
                transform: translate3d(-0.01em, 0, 0);
            }
            30% {
                transform: translate3d(0.01em, 0, 0);
            }
            32% {
                transform: translate3d(-0.01em, 0, 0);
            }
            34% {
                transform: translate3d(0.01em, 0, 0);
            }
            36% {
                transform: translate3d(-0.01em, 0, 0);
            }
            38% {
                transform: translate3d(0.01em, 0, 0);
            }
            40% {
                transform: translate3d(-0.01em, 0, 0);
            }
            42% {
                transform: translate3d(0.01em, 0, 0);
            }
            44% {
                transform: translate3d(-0.01em, 0, 0);
            }
            46% {
                transform: translate3d(0em, 0, 0);
            }
        }

    </style>
    {{--    // Thêm vào sau--}}

    <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">
</head>
<body>
@include('layouts.menu')
<div id="root"></div>
@include('layouts.common.croll')
<div class="gap"></div>
@include('layouts.footer')
</body>

{{--// Thêm vào sau--}}
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/toast/toastr.min.js"></script>
{{--CROLL TOP--}}
<script>
    window.$ = window.jQuery = $;

    $('' +
        '#cuon').click(function () {
        $('body,html').animate({scrollTop: 0}, 600);
        return false;
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() === 0) {
            $('#cuon').stop(false, true).fadeOut(300);
        } else {
            $('#cuon').stop(false, true).fadeIn(300);
        }
    });
</script>
{{--// Thêm vào sau--}}

<script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
</html>
@extends('login.index')

@section('content')
    <div class="wrapper fadeInDown">
        <div id="formContent">
            <!-- Tabs Titles -->
            <!-- Icon -->
            <div class="fadeIn first">
                <img class="rounded"
                     width="350px"
                     src="/logo/logoknl-login.png"
                     alt="Logo Tìm Trọ"
                />
            </div>

            @if(count($errors) > 0)
                <div class="alert alert-danger text-center" role="alert">
                    @foreach($errors->all() as $err)
                        <b>{{$err}}<br></b>
                    @endforeach
                </div>
            @endif
            @if(Session::has('register-success'))
                <div class="alert alert-success text-center" role="alert">
                    <b>{{Session::get('register-success')}}</b>
                </div>
        @endif

        <!-- Login Form -->
            <form action="{{route('register.post')}}" method="POST">
                <input type="text" id="login" class="fadeIn second" name="username" placeholder="Tài khoản...">
                <input type="password" id="password" class="fadeIn third" name="password" placeholder="Mật khẩu...">
                <input type="password" id="password" class="fadeIn third" name="re_password" placeholder="Nhập lại ật khẩu...">
                <input type="text" id="login" class="fadeIn second" name="email" placeholder="Email...">
                <input type="text" id="login" class="fadeIn second" name="phone" placeholder="Số điện thoại...">
                <input type="text" id="login" class="fadeIn second" name="mobile" placeholder="Di động...">
                <input type="text" id="login" class="fadeIn second" name="last_name" placeholder="Họ...">
                <input type="text" id="login" class="fadeIn second" name="first_name" placeholder="Tên...">
                <div class="form-group">
                    <select class="form-control" name="sex" style="width: 380px; margin-left: 35px">
                        <option value="1">Nam</option>
                        <option value="0">Nữ</option>
                    </select>
                </div>
{{--                <div class="wrap-input100 validate-input" data-validate="Enter captcha">--}}
{{--                    <div class="row">--}}
{{--                        <div class="col col-md-4 col-sm-4 col-xs-4 captcha">--}}
{{--                            <span class="ml-3">{!! captcha_img() !!}</span>--}}
{{--                        </div>--}}
{{--                        <div class="col col-md-4 col-sm-4 col-xs-4"></div>--}}
{{--                        <div class="col col-md-4 col-sm-4 col-xs-4">--}}
{{--                            <button class="btn btn-info" type="button" id="refresh-captcha" name="refresh-captcha">Làm--}}
{{--                                mới--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <input class="input100" type="text" name="captcha" placeholder="Captcha của bạn...">--}}
{{--                </div>--}}
                <div class="mt-3 mb-3 w-50" style="margin-left: 110px">
                    <button class="btn btn-block btn-primary" type="submit">Đăng kí</button>
                </div>
                {{csrf_field()}}
            </form>
            <div class="text-center p-t-90">
                <a class="txt1" href="{{route('login.get')}}">
                    Đã có tài khoản?
                </a>
            </div>
            <!-- Remind Passowrd -->
            <div id="formFooter">
                <a class="underlineHover">© Bản quyền 2020?</a>
            </div>

        </div>
    </div>
@endsection
@section('js_footer')
    @parent
    <script src="https://code.jquery.com/jquery-3.4.1.js"></script>

    <script>
        $('#refresh-captcha').click(function () {
            console.log('da refresh captcha!')

            $.ajax({
                type: 'GET',
                url: '{{route('refresh.captcha')}}',
                success: function (data) {
                    $('.captcha span').html(data);
                }
            });
        });
    </script>
@endsection
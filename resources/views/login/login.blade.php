@extends('login.index')
@section('css_head')
    <style>
        .changeColorBtn {
            background: #D2691E;
            color: white;
        }
    </style>
@endsection
@section('content')
    <div class="wrapper fadeInDown">
        <div id="formContent">
            <!-- Tabs Titles -->
            <!-- Icon -->
            <div class="fadeIn first">
                <a href="/">
                    <img class="rounded"
                         width="350px"
                         src="/logo/logoknl-login.png"
                         alt="Logo Tìm Trọ"
                    />
                </a>
            </div>

            @if(Session::has('flag'))
                <div class="alert alert-{{Session::get('flag')}} text-center" role="alert">
                    <b>{{Session::get('login-notification')}}</b>
                </div>
        @endif
            @if(Session::has('changePass'))
                <div class="alert alert-{{Session::get('changePass')}} text-center" role="alert">
                    <b>{{Session::get('password-notification')}}</b>
                </div>
        @endif

        <!-- Login Form -->
            <form action="{{route('login.post')}}" method="POST">
                <input type="text" id="login" class="fadeIn second" name="username" placeholder="Tài khoản...">
                <input type="password" id="password" class="fadeIn third" name="password" placeholder="Mật khẩu...">
                <div class="mt-3 mb-3 w-50" style="margin-left: 110px">
                    <button class="btn btn-block btn-warning changeColorBtn" type="submit">Đăng nhập</button>
                </div>
                {{csrf_field()}}
            </form>
            <div class="mb-3">
                <a class="btn btn-outline-warning txt1" href="{{route('get.password.reset')}}">
                    Quên mật khẩu?
                </a>

                <a class="btn btn-outline-warning txt1" href="{{route('register.get')}}">
                    Đăng kí?
                </a>
            </div>
            <!-- Remind Passowrd -->
            <div id="formFooter">
                <a class="underlineHover">© Bản quyền thuộc Young Generation 2020?</a>
            </div>

        </div>
    </div>
@endsection
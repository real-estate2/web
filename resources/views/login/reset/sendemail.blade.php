@extends('login.index')

@section('content')
    <div class="wrapper fadeInDown">
        <div id="formContent">
            <!-- Tabs Titles -->
            <!-- Icon -->
            <div class="fadeIn first">
                <a href="/">
                    <img class="rounded"
                         width="350px"
                         src="/logo/logoknl-login.png"
                         alt="Logo"
                    />
                </a>
            </div>

            @if($errors->has('email'))
                <div class="alert alert-success text-center" role="alert">
                    <b>{{$errors->first('email')}}</b>
                </div>
            @endif
            @if(Session::has('flagMail'))
                <div class="alert alert-{{Session::get('flagMail')}} text-center" role="alert">
                    <b>{{Session::get('mail-notification')}}</b>
                </div>
        @endif

        <!-- Login Form -->
            <form method="POST">
                <input type="text" id="email" class="fadeIn second" name="email" placeholder="Email lấy lại mật khẩu...">
                <div class="mt-3 mb-3 w-50" style="margin-left: 110px">
                    <button class="btn btn-block btn-warning changeColorBtn" type="submit">Đồng ý</button>
                </div>
                {{csrf_field()}}
            </form>
            <div class="mb-3">
                <a class="btn btn-outline-warning txt1" href="{{route('login.get')}}">
                    Đã có tài khoản?
                </a>

                <a class="btn btn-outline-warning txt1" href="{{route('register.get')}}">
                    Đăng kí?
                </a>
            </div>
            <!-- Remind Passowrd -->
            <div id="formFooter">
                <a class="underlineHover">© Bản quyền thuộc Young Generation 2020?</a>
            </div>
        </div>
    </div>
@endsection
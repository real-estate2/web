<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    @yield('css_head')
</head>
<body>

<div>
    @yield('content')
</div>

@yield('js_footer')
</body>
</html>
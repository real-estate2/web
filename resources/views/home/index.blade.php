@extends('layouts.master')
@section('css_head')
    @parent
    <link rel="stylesheet" href="css/home.css">
@endsection
@section('content')
    <div class="s01">
        <form action="{{route('search')}}" method="GET">
            <div class="inner-form">
                <div class="input-field first-wrap">
                    <input type="search" id="query" name="query"
                           value="{{request()->input('query')}}" placeholder="Bạn muốn tìm...?"/>
                </div>
                <div class="input-field third-wrap">
                    <button class="btn-search" type="submit">Tìm kiếm</button>
                </div>
            </div>
        </form>
    </div>

    <section class="mt-5 mb-5">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="text-center mb-2 animated fadeInUp">
                        <span style="font-size: 3em; color: Tomato;">
                            <i class="fas fa-business-time"></i>
                        </span>
                        <h5><span class="counter">Cập nhật sản phẩm liên tục</span></h5>
                    </div>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="text-center mb-2 animated bounce">
                        <span style="font-size: 3em; color: Tomato;">
                            <i class="fas fa-users"></i>
                        </span>

                        <h5><span class="counter">Đội ngũ chuyên nghiệp</span></h5>
                    </div>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="text-center mb-2 animated fadeInUp">
                        <span style="font-size: 3em; color: Tomato;">
                            <i class="fas fa-money-check-alt"></i>
                        </span>

                        <h5><span class="counter">Thanh toán dễ dàng</span></h5>
                    </div>
                </div>

                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
                    <div class="text-center mb-2 animated fadeInUp">
                        <span style="font-size: 3em; color: Tomato;">
                            <i class="far fa-handshake"></i>
                        </span>

                        <h5><span class="counter">Uy tính và trách nhiệm</span></h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="layerbackground">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <h5>DỰ ÁN CỦA CHÚNG TÔI</h5>
                        <div class="ml-auto">
                        </div>
                    </nav>
                    <hr>
                    <h6>Sản phẩm của chúng tôi đến từ các tập đoàn lớn trong và ngoài nước.</h6>
                    <div class="row">
                        <div class="grid">
                            @if(count($apartment) > 0)
                                <a href="{{route('project.productCategory', ['id' => $apartment->pluck('product_category')[0]])}}">
                                    <figure class="effect-bubba">
                                        <img src="banner/can-ho.jpg" alt="img02"/>
                                        <figcaption>
                                            <h2>CĂN HỘ BÁN</h2>
                                            <p>{{count($apartment)}} DỰ ÁN</p>
                                        </figcaption>
                                    </figure>
                                </a>
                            @endif
                            @if(count($apartment_rent) > 0)
                                <a href="{{route('project.productCategory', ['id' => $apartment_rent->pluck('product_category')[0]])}}">
                                    <figure class="effect-bubba">
                                        <img src="banner/can-ho.jpg" alt="img02"/>
                                        <figcaption>
                                            <h2>CĂN HỘ THUÊ</h2>
                                            <p>{{count($apartment_rent)}} DỰ ÁN</p>
                                        </figcaption>
                                    </figure>
                                </a>
                            @endif
{{--                            @if(count($sell) > 0)--}}
{{--                                <a href="{{route('project.productCategory', ['id' => $sell->pluck('product_category')[0]])}}">--}}
{{--                                    <figure class="effect-bubba">--}}
{{--                                        <img src="banner/nha-pho-du-an.jpg" alt="img16"/>--}}
{{--                                        <figcaption>--}}
{{--                                            <h2>Cần bán</h2>--}}
{{--                                            <p>{{count($sell)}} DỰ ÁN</p>--}}
{{--                                        </figcaption>--}}
{{--                                    </figure>--}}
{{--                                </a>--}}
{{--                            @endif--}}
                            @if(count($land_project) > 0)
                                <a href="{{route('project.productCategory', ['id' => $land_project->pluck('product_category')[0]])}}">
                                    <figure class="effect-bubba">
                                        <img src="banner/du-an-dat-nen.jpg" alt="img16"/>
                                        <figcaption>
                                            <h2>DỰ ÁN ĐẤT NỀN</h2>
                                            <p>{{count($land_project)}} DỰ ÁN</p>
                                        </figcaption>
                                    </figure>
                                </a>
                            @endif
                            @if(count($resort) > 0)
                                <a href="{{route('project.productCategory', ['id' => $resort->pluck('product_category')[0]])}}">
                                    <figure class="effect-bubba">
                                        <img src="banner/dat-le.jpg" alt="img16"/>
                                        <figcaption>
                                            <h2>KHU NGHỈ DƯỠNG</h2>
                                            <p>{{count($resort)}} DỰ ÁN</p>
                                        </figcaption>
                                    </figure>
                                </a>
                            @endif

                            @if(count($storehouse) > 0)
                                <a href="{{route('project.productCategory', ['id' => $storehouse->pluck('product_category')[0]])}}">
                                    <figure class="effect-bubba">
                                        <img src="banner/nha-pho-thu-dau-mot.jpg" alt="img16"/>
                                        <figcaption>
                                            <h2>NHÀ XƯỞNG BÁN</h2>
                                            <p>{{count($storehouse)}} DỰ ÁN</p>
                                        </figcaption>
                                    </figure>
                                </a>
                            @endif
                            @if(count($storehouse_rent) > 0)
                                <a href="{{route('project.productCategory', ['id' => $storehouse_rent->pluck('product_category')[0]])}}">
                                    <figure class="effect-bubba">
                                        <img src="banner/nha-pho-thu-dau-mot.jpg" alt="img16"/>
                                        <figcaption>
                                            <h2>NHÀ XƯỞNG THUÊ</h2>
                                            <p>{{count($storehouse_rent)}} DỰ ÁN</p>
                                        </figcaption>
                                    </figure>
                                </a>
                            @endif
                            @if(count($land) > 0)
                                <a href="{{route('project.productCategory', ['id' => $land->pluck('product_category')[0]])}}">
                                    <figure class="effect-bubba">
                                        <img src="banner/nha-pho-thu-dau-mot.jpg" alt="img16"/>
                                        <figcaption>
                                            <h2>ĐẤT BÁN</h2>
                                            <p>{{count($land)}} DỰ ÁN</p>
                                        </figcaption>
                                    </figure>
                                </a>
                            @endif
                            @if(count($home_street) > 0)
                                <a href="{{route('project.productCategory', ['id' => $home_street->pluck('product_category')[0]])}}">
                                    <figure class="effect-bubba">
                                        <img src="banner/nha-pho-thu-dau-mot.jpg" alt="img16"/>
                                        <figcaption>
                                            <h2>NHÀ MẶT PHỐ</h2>
                                            <p>{{count($home_street)}} DỰ ÁN</p>
                                        </figcaption>
                                    </figure>
                                </a>
                            @endif
                            @if(count($home_private) > 0)
                                <a href="{{route('project.productCategory', ['id' => $home_private->pluck('product_category')[0]])}}">
                                    <figure class="effect-bubba">
                                        <img src="banner/nha-pho-thu-dau-mot.jpg" alt="img16"/>
                                        <figcaption>
                                            <h2>THUÊ NHÀ RIÊNG</h2>
                                            <p>{{count($home_private)}} DỰ ÁN</p>
                                        </figcaption>
                                    </figure>
                                </a>
                            @endif
                            @if(count($office) > 0)
                                <a href="{{route('project.productCategory', ['id' => $office->pluck('product_category')[0]])}}">
                                    <figure class="effect-bubba">
                                        <img src="banner/nha-pho-thu-dau-mot.jpg" alt="img16"/>
                                        <figcaption>
                                            <h2>THUÊ VĂN PHÒNG</h2>
                                            <p>{{count($office)}} DỰ ÁN</p>
                                        </figcaption>
                                    </figure>
                                </a>
                            @endif
                        </div>
                    </div>
                    <div class="text-center mt-3">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
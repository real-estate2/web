@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-5 col-md-12 col-sm-12 col-sx-12 mt-3">
                <img src="http://127.0.0.1:8000/images/agency/{{$item->thumbnails}}" class="img-thumbnail">
            </div>
            <div class="col-lg-7 mt-3" id="utilities">
                <h1>{{$item->contact_name}}</h1>
                <p>{{$item->position}}</p>
                <ul>
                    <li>Số điện thoại: {{$item->contact_mobile}}</li>
                    <li>Email: {{$item->contact_email}}</li>
                    <li>Công ty: {{$item->company}}</li>
                </ul>
                <button class="btn btn-warning text-white"><i class="fas fa-mobile-alt"></i> {{$item->contact_mobile}}</button>
                <button class="btn btn-dark text-white"><i class="fas fa-mail-bulk"></i> Liên hệ tư vấn</button>
            </div>
        </div>

        <div class="mt-5"></div>
        <div class="row">
            <div class="col-lg-5">
                <h2>Kinh nghiệm</h2>
                <p>{{$item->introduce}}</p>
            </div>
            <div class="col-lg-7">
                <h2>Giới thiệu</h2>
                <p>{{$item->experience}}</p>
            </div>
        </div>
    </div>
@endsection
@extends('layouts.master')
@section('css_head')
    @parent
    <style>
        .changeImg img {
            opacity: 0.7;
        }
        .map-responsive {
            width: 100%;
            height: 600px;
            max-height: 800px;
        }
    </style>
@endsection
@section('content')
    <section>
        <div class="container">
            <div class="row">
            <div class="col-12 col-sm-12 col-lg-12">
            <nav class="navbar navbar-expand-lg">
                        <h5>Về Chúng Tôi: <strong> Young Generation</strong> </h5>
                    </nav>
                    <hr>
                    <h6>Các nhân sự bật nhất trong công ty.</h6>
                    <div class="row">
                <div class="col-md-6">
                    <p>Tên công ty : Công ty TNHH sàn môi giới và bất động sản <strong>Young Generation</strong></p>
                    <p>Tên viết tắt : <strong>YG Corp</strong> ( Young Generation Corporation)</p>
                    <p>Ý nghĩa của từ <strong> Young Generation </strong> và sứ mệnh của chúng tôi: <strong> Young Generation </strong> được rút gọn từ Slogan của chúng tôi: <strong>‘Home for next generation’</strong>. Và slogan của chúng tôi bắt nguồn từ sứ mệnh kinh doanh của chúng tôi: To build and provide the better home for next generation ( Xây dựng và gửi trao ngôi nhà tốt hơn cho thế hệ kế thừa).</p>
                    <p><strong> Young Generation </strong> vừa là đơn vị kinh doanh tạo ra và đóng góp những giá trị cho khách hàng và cộng đồng, và đồng thời cũng là nơi làm việc, học tập và phát triển cho thế hệ trẻ. Những người sẽ trở thành lực lượng nòng cốt giúp Bình Dương phát triển tích cực và bền vững.</p>
                    <p>Chúng tôi tập trung vào những lĩnh vực kinh doanh chính sau:

<p>– Tư vấn chiến lược phát triển dự án bất động sản</p>

<p>– Tư vấn chiến lược sales và marketing dự án bất động sản (căn hộ, nhà phố, biệt thự)</p>

<p>– Môi giới mua bán bất động sản (căn hộ, nhà phố, biệt thự, đất nền)</p>

<p>– Môi giới cho thuê (retail, commercial area, serived apartment)</p>
 
                    <p>Tập thể  <strong> Young Generation </strong>  là những con người năng động, tin cậy và trách nhiệm.</p>
                    <p>Chúng tôi là đội ngũ ở độ tuổi rất trẻ, tràn đầy năng lượng, làm việc với sự đam mê.</p>
                    <p>Chúng tôi xem sự hài lòng của khách hàng là niềm tự hào của chúng tôi.</p>
                </div>
                <div class="col-md-4">
                    <img src="https://homenext.vn/wp-content/uploads/2019/09/thinhgia8.png" />
                </div>
            </div>
        </div>
        </div>
        </div>
    </div>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <h5>NHÂN SỰ CỦA YOUNG GENERATION</h5>
                    </nav>
                    <hr>
                    <h6>Các nhân sự bật nhất trong công ty.</h6>
                    <div class="row">
                        @foreach($agency as $item)
                            <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12 mt-3">
                                <div class="card">
                                    <a href="{{route('agency.detail', ['slug' => $item->slug, 'id' => $item->id])}}"
                                       data-toggle="tooltip" data-placement="right" title="{{$item->title}}"
                                       class="" style="text-decoration: none">
                                        <img src="http://127.0.0.1:8000/images/agency/{{$item->thumbnails}}"
                                             height="200"
                                             class="card-img-top">
                                        <div class="card-body">
                                            <h6 class="card-title text-info crop-text" style="text-transform:uppercase">
                                                {{$item->contact_name}}
                                            </h6>
                                            <div class="card-subtitle mb-2 text-muted">
                                                {{$item->contact_email}}
                                            </div>
                                            <div class="card-subtitle mb-2 text-muted">
                                                {{$item->contact_mobile}}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center mt-3">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <h3>Thông tin liên hệ</h3>
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-danger">
                            <i class="fas fa-phone-volume"></i>
                            Tổng đài miễn phí
                        </h5>
                        <ul>
                            <li>Tư vấn mua hàng: 1800 6867</li>
                            <li>Chăm sóc khách hàng: 1800 6865</li>
                            <li>Email: cskh@giautran.vn</li>
                        </ul>
                        <h5 class="card-title text-danger">
                            <i class="far fa-envelope"></i>
                            Liên hệ báo giá
                        </h5>
                        <ul>
                            <li>Email: YoungGeneration@gmail.vn</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Tham gia Fanpage Young Genaration trên facebook để hưởng nhiều ưu
                            đãi</h5>
                        <a href="#">
                            <img src="/logo/banner.jpg"
                                 class="img-fluid" alt="Responsive image">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <!--Google map-->
            <div class="mt-3 map-responsive">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6524.087993089738!2d106.672488630414!3d10.980559266718492!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d12739bb468f%3A0xe2bf4f397d0aa76!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBUaOG7pyBE4bqndSBN4buZdA!5e0!3m2!1svi!2s!4v1584085921396!5m2!1svi!2s" width="100%" height="90%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>

            <!--Google Maps-->
        </div>
    </div>
@endsection
@extends('layouts.master')
@section('css_head')
    @parent
    <style>
        .changeImg img {
            opacity: 0.7;
        }
    </style>
@endsection
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <h5>DỰ ÁN</h5>
                    </nav>
                    <hr>
                    <h6>Các dự án bật nhất trong khu vực.</h6>
                    <div class="row">
                        @foreach($category as $item)
                            <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12 mt-3">
                                <div class="card">
                                    <a href="{{route('project.index', ['id' => $item->id])}}"
{{--                                    <a href=""--}}
                                       data-toggle="tooltip" data-placement="right" title="{{$item->name}}"
                                       class="" style="text-decoration: none">
                                        <img src="http://127.0.0.1:8000/images/categories/{{$item->thumbnails}}"
                                             height="200"
                                             class="card-img-top">
                                        <div class="card-body">
                                            <h6 class="card-title text-info crop-text" style="text-transform:uppercase">
                                                {{$item->name}}
                                            </h6>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center mt-3">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@extends('layouts.master')
@section('css_head')
    @parent
    <style>
        .changeImg img {
            opacity: 0.7;
        }
    </style>
@endsection
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <h5>{{$project->name}}</h5>
                    </nav>
                    <hr>                
                    <div class="row">
                        <div class="col-md-8">
                            <p>{!!$project->description!!}</p>
                            <br>
                            <h5><strong>Vị trí dự án:</strong> {{$district->full_address}}</h5>
                        </div>
                        <div class="col-md-4">
                            <img width="400px" src="http://127.0.0.1:8000/images/categories/{{$project->thumbnails}}"/>
                        </div>
                    </div>
                    <div class="mt-5"></div>
                    @if(count($post) > 0)

                    <nav class="navbar navbar-expand-lg">
                        <h5>Các bài viết liên quan đến dự án</h5>
                    </nav>
                    <hr>        
                    <div class="row">

                        @foreach($post as $item)
                            <div class="col-lg-3 col-md-6 col-sm-6 col-sx-12 mt-3">
                                <div class="card">
                                    <a href="{{route('project.details', ['slug' => $item->slug, 'id' => $item->id])}}"
                                       data-toggle="tooltip" data-placement="right" title="{{$item->title}}"
                                       class="" style="text-decoration: none">
                                        <img src="{{$item->thumbnails}}"
                                             height="200"
                                             class="card-img-top">
                                             @if($item->post_type == 1)
                                             <!-- <div class="top-right">{{$item->getPostTypeString()}}</div> -->
                                             <div class="top-right" style="margin-top: -20px; margin-right: -20px">
                                                <img src="/logo/hot.png" />
                                             </div>
                                             @endif
                                        <div class="card-body">
                                            <h6 class="card-title text-info crop-text" style="text-transform:uppercase">
                                                {{$item->title}}
                                            </h6>
                                            <div class="card-subtitle mb-2 text-muted">
                                                <i class="fas fa-user"></i>
                                                <span style="float: right;">{{$item->users->last_name}} {{$item->users->first_name}}</span>
                                            </div>
                                            <div class="card-subtitle mb-2 text-muted">
                                                <i class="fas fa-map-marked-alt"></i>
                                                <span style="float: right;">{{$item->projects['name']}}</span>

                                            </div>
                                            <div class="card-subtitle mb-2 text-muted">
                                                <i class="far fa-money-bill-alt"></i>
                                                <span style="float: right;" class="text-danger">
                                                    {{ number_format($item->price) }} {{$item->getProductPriceUnitString}}
                                                </span>
                                            </div>
                                            <div class="card-subtitle mb-2 text-muted">
                                                <i class="fas fa-expand"></i>
                                                <span style="float: right;">{{$item->area}} m<sup>2</sup></span>

                                            </div>
                                            <div class="card-subtitle mb-2 text-muted">
                                                <i class="fas fa-th-large"></i>
                                                <span style="float: right;">{{$item->getProductCategoryString}}</span>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    @endif

                    </div> 
                    <div class="text-center mt-3">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
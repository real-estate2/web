<div class="mb-2">
    <h5><i class="fab fa-youtube"></i> VIDEO</h5>
    <div class="embed-responsive embed-responsive-21by9">
        <iframe class="embed-responsive-item" src="{{$data->youtube}}" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen></iframe>
    </div>
</div>
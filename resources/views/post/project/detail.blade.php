@extends('layouts.master')
@section('css_head')
    @parent
    <link rel="stylesheet" href="https://d19vzq90twjlae.cloudfront.net/leaflet-0.7.3/leaflet.css" />
    <style>
        .changeImg img {
            opacity: 0.7;
        }
        #mapid {
            width: auto;
            height: 400px;
            border: 0;
            z-index: 0;
        }
    </style>
@endsection
@section('content')
    <section class="mb-5">
        <div class="container">
        @if(Session::has('customer'))
                <div class="alert alert-{{Session::get('customer')}} text-center" role="alert">
                    <b>{{Session::get('customer-notification')}}</b>
                </div>
        @endif
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <h5 style="text-transform: uppercase">{{$data->title}}</h5>
                    </nav>
                    <hr>
{{--                    <h6>Sản phẩm của chúng tôi đến từ các tập đoàn lớn trong và ngoài nước.</h6>--}}
                    <div class="text-center mt-3">
                    </div>
                </div>
                <div class="col-lg-8">
                    <h6 class="text-right">
                        <i class="far fa-clock"></i> {{$data->start_date}}
                    </h6>
                    @include('post.project.common.gallery')
                </div>
                <div class="col-lg-4">
                    @include('post.project.common.info_basic')
                    <hr>
                    @include('post.project.common.sign_up_for')
                </div>
            </div>

            <hr>
            @include('post.project.common.content')
            @include('post.project.common.utilities')
            <div class="mb-2">
                <h5><i class="fas fa-map-marked-alt"></i> BẢN ĐỒ</h5>
                <div id="mapid"></div>
            </div>
            @include('post.project.common.video')
            <hr>
            @include('post.project.common.related_news')
            <div class="text-center mt-3">
            </div>
        </div>
    </section>
@endsection
@section('js_footer')
    @parent
    <script src="https://d19vzq90twjlae.cloudfront.net/leaflet-0.7.3/leaflet.js"></script>
    <script>
        var nearBy = [
                @foreach($nearBy as $p)
            {lng: {{$p->longitude}}, lat: {{$p->latitude}}},
            @endforeach
        ];

        var mymap = L.map('mapid').setView([{{ $data->latitude}}, {{ $data->longitude }}], 14);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1
        }).addTo(mymap);

        L.marker([{{ $data->latitude}}, {{ $data->longitude }}]).addTo(mymap)
            .bindPopup("<b>{{$data->title}}</b").openPopup();

        L.circle([{{ $data->latitude}}, {{ $data->longitude }}], 2000, {
            color: 'red',
            fillColor: '#F5FFFA',
            fillOpacity: 0.5
        }).addTo(mymap).bindPopup("I am a circle.");
        var popup = L.popup();

        // TIỆN ÍCH XUNG QUANH
        var nearIcon = L.icon({
            iconUrl: '/icon/nearIcon.png',
            iconSize: [24,24],
            shadowSize: [24,24],
            shadowAnchor: [4,62],
            popupAnchor: [-3,-76]
        });
        for (let i = 0, l = nearBy.length; i < l; i++) {
            L.marker([nearBy[i].lat, nearBy[i].lng], {icon: nearIcon}).addTo(mymap)
        }
    </script>
@endsection
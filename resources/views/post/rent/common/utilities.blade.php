<div class="mb-2">
    <h5><i class="fas fa-street-view"></i> CÁC TIỆN ÍCH</h5>
    <div class="mt-2 mb-2">
        <table class="table">
            <thead>
            <tr>
                <th>Tên tiện ích</th>
                <th>Khoảng cách</th>
            </tr>
            </thead>
            <tbody>
            @foreach($nearBy as $p)
                <tr>
                    <td>{{$p->name}}</td>
                    <td>{{round($p->distance*1000)}} m</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
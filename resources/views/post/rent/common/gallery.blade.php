@section('css_head')
    @parent
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.19/css/lightgallery.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightslider/1.1.6/css/lightslider.min.css">
@endsection

<ul id="imageGallery" class="gallery list-unstyled">
    @foreach($list_image as $image)
        <li data-thumb="http://127.0.0.1:8000/images/post/{{ $image->xs }}"
            data-src="http://127.0.0.1:8000/images/post/{{ $image->o }}">
            <img src="http://127.0.0.1:8000/images/post/{{ $image->o }}" style="height: 100%;max-width: 100%"/>
        </li>
    @endforeach
</ul>

@section('js_footer')
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.2.19/js/lightgallery-all.min.js"></script>
    <script src="../../js/lightslider.js"></script>


    <script>
        $('#imageGallery').lightSlider({
            gallery: true,
            item: 1,
            loop: true,
            thumbItem: 9,
            slideMargin: 0,
            enableDrag: false,
            speed: 1000,
            auto: true,
            currentPagerPosition: 'left',
            onSliderLoad: function (el) {
                el.lightGallery({
                    selector: '#imageGallery .lslide'
                });
            }
        });
    </script>

@endsection
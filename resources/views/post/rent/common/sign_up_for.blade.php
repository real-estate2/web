<a type="button" style="background: #D2691E" class="btn btn-danger mb-2 w-100 text-white">
    <b>Gọi cho chuyên viên</b>
</a>
<a data-toggle="modal" href="#modalWatchLive" style="background: #D2691E" data-target="#modalWatchLive" type="button"
   class="btn btn-info mb-2 w-100">
    <b>Đăng kí xem trực tiếp</b>
</a>
<a data-toggle="modal" href="#modalGetPriceList" style="background: #D2691E" data-target="#modalGetPriceList" type="button"
   class="btn btn-info mb-2 w-100">
    <b>Đăng kí nhận bảng giá</b>
</a>
<a data-toggle="modal" href="#modalAdditionalQuestion" style="background: #D2691E" data-target="#modalAdditionalQuestion" type="button"
   class="btn btn-info mb-2 w-100">
    <b>Hỏi thêm thông tin</b>
</a>

<div class="modal fade" id="modalWatchLive" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Đăng kí xem trực tiếp</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('watchLive.post', ['id' => $data->id])}}" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Họ tên</label>
                        <input type="text" class="form-control" name="contact_name" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Giới tính</label>
                        <select class="form-control" name="sex" id="exampleFormControlSelect1">
                            <option value="0">Nam</option>
                            <option value="1">Nữ</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Số điện thoại</label>
                        <input type="text" class="form-control" name="contact_mobile" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Địa chỉ Email</label>
                        <input class="form-control" name="contact_email" id="message-text"/>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Ngày đăng kí xem trực tiếp</label>
                        <input type="date" class="form-control" name="live_date" id="recipient-name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Gửi thông tin</button>
                </div>
                {{csrf_field()}}
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalGetPriceList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Đăng kí nhận bảng giá</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('getPriceList.post', ['id' => $data->id])}}" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Họ tên</label>
                        <input type="text" class="form-control" name="contact_name" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Giới tính</label>
                        <select class="form-control" name="sex" id="exampleFormControlSelect1">
                            <option value="0">Nam</option>
                            <option value="1">Nữ</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Số điện thoại</label>
                        <input type="text" class="form-control" name="contact_mobile" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Địa chỉ Email</label>
                        <input class="form-control" name="contact_email" id="message-text"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Gửi thông tin</button>
                </div>
                {{csrf_field()}}
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAdditionalQuestion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Hỏi thêm thông tin</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('additionalQuestion.post', ['id' => $data->id])}}" method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Họ tên</label>
                        <input type="text" class="form-control" name="contact_name" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Giới tính</label>
                        <select class="form-control" name="sex" id="exampleFormControlSelect1">
                            <option value="0">Nam</option>
                            <option value="1">Nữ</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Số điện thoại</label>
                        <input type="text" class="form-control" name="contact_mobile" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Địa chỉ Email</label>
                        <input class="form-control" name="contact_email" id="message-text"/>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Lời nhắn</label>
                        <textarea class="form-control" name="question" id="message-text"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                    <button type="submit" class="btn btn-primary">Gửi thông tin</button>
                </div>
                {{csrf_field()}}
            </form>
        </div>
    </div>
</div>
<h5 style="text-transform: uppercase">{{$data->title}}</h5>
<h6><span class="text-danger">{{$data->address}}</span></h6>
<table class="table">
    <tbody>
    <b>Thông tin chung</b>
    <tr>
        <td>Giá:</td>
        <td class="text-right">{{ number_format($item->price, 2) }} {{$item->getProductPriceUnitString}}</td>
    </tr>
    <tr>
        <td>Diện tích:</td>
        <td class="text-right">{{$data->area}} m2</td>
    </tr>
    <tr>
        <td>Loại hình:</td>
        <td class="text-right">{{$data->getProductCategoryString}}</td>
    </tr>
    </tbody>
</table>

<!-- Large modal -->
<button type="button" class="btn btn-primary w-100" data-toggle="modal"
        data-target=".bd-example-modal-lg">
    Thông tin chi tiết
</button>

<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <h5 style="text-transform: uppercase">{{$data->title}}</h5>
                <img src="" class="mx-auto d-block">
                <table class="table">
                    <tbody>
                    <tr>
                        <td>
                            <h5>Thông tin tổng quan</h5>
                        </td>
                    </tr>
                    <tr>
                        <td>Tên dự án :</td>
                        <td class="text-right">{{$data->title}}</td>
                    </tr>
                    <tr>
                        <td>Địa chỉ :</td>
                        <td class="text-right">{{$data->address}}</td>
                    </tr>
                    <tr>
                        <td>Tổng diện tích :</td>
                        <td class="text-right">{{$data->area}} {{$data->area_unit}}</td>
                    </tr>
                    <tr>
                        <td>Loại hình mua bán :</td>
                        <td class="text-right">{{$data->toArray['product_type_string']}}</td>
                    </tr>
                    <tr>
                        <td>Loại hình phát triển :</td>
                        <td class="text-right">{{$data->toArray['product_category_string']}}</td>
                    </tr>


                    <tr>
                        <td><h5>Cấu trúc</h5></td>
                    </tr>
                    <tr>
                        <td>Hướng nhà :</td>
                        <td class="text-right">{{$data->toArray['main_direction_string']}}</td>
                    </tr>
                    <tr>
                        <td>Phòng ngủ :</td>
                        <td class="text-right" width="340">{{$data->nbr_bed_room}}</td>
                    </tr>
                    <tr>
                        <td>Toilet :</td>
                        <td class="text-right">{{$data->nbr_rest_room}}</td>
                    </tr>
                    <tr>
                        <td>Số tầng :</td>
                        <td class="text-right">{{$data->nbr_floor}}</td>
                    </tr>
                    <tr>
                        <td>Mặt tiền :</td>
                        <td class="text-right">{{$data->front_street_width}}</td>
                    </tr>
                    <tr>
                        <td>Đường vào :</td>
                        <td class="text-right">{{$data->behind_street_width}}</td>
                    </tr>
                    <tr>
                        <td><h5>Vị trí bản đồ</h5></td>
                    </tr>
                    <tr>
                        <td>Kinh độ :</td>
                        <td class="text-right">{{$data->longitude}}</td>
                    </tr>
                    <tr>
                        <td>Vĩ độ :</td>
                        <td class="text-right">{{$data->latitude}}</td>
                    </tr>
                    <tr>
                        <td><h5>Liên hệ</h5></td>
                    </tr>
                    <tr>
                        <td>Họ tên :</td>
                        <td class="text-right">{{$data->contact_name}}</td>
                    </tr>
                    <tr>
                        <td>Địa chỉ :</td>
                        <td class="text-right">{{$data->contact_address}}</td>
                    </tr>
                    <tr>
                        <td>Điện thoại :</td>
                        <td class="text-right">{{$data->contact_mobile}}</td>
                    </tr>
                    <tr>
                        <td>Email :</td>
                        <td class="text-right">{{$data->contact_email}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
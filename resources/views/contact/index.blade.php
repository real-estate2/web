@extends('layouts.master')

@section('css_head')
    @parent
    <style>
        .map-responsive {
            width: 100%;
            height: 600px;
            max-height: 800px;
        }
    </style>
@endsection

<div id="result-map"></div>

@section('content')
    <div class="container">
        <h3>Thông tin liên hệ</h3>
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title text-danger">
                            <i class="fas fa-phone-volume"></i>
                            Tổng đài miễn phí
                        </h5>
                        <ul>
                            <li>Tư vấn mua hàng: 1800 6867</li>
                            <li>Chăm sóc khách hàng: 1800 6865</li>
                            <li>Email: cskh@giautran.vn</li>
                        </ul>
                        <h5 class="card-title text-danger">
                            <i class="far fa-envelope"></i>
                            Liên hệ báo giá
                        </h5>
                        <ul>
                            <li>Email: YoungGeneration@gmail.vn</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Tham gia Fanpage Young Genaration trên facebook để hưởng nhiều ưu
                            đãi</h5>
                        <a href="#">
                            <img src="/logo/banner.jpg"
                                 class="img-fluid" alt="Responsive image">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <!--Google map-->
            <div class="mt-3 map-responsive">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6524.087993089738!2d106.672488630414!3d10.980559266718492!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3174d12739bb468f%3A0xe2bf4f397d0aa76!2zVHLGsOG7nW5nIMSQ4bqhaSBo4buNYyBUaOG7pyBE4bqndSBN4buZdA!5e0!3m2!1svi!2s!4v1584085921396!5m2!1svi!2s" width="100%" height="90%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            </div>

            <!--Google Maps-->
        </div>
    </div>
@endsection


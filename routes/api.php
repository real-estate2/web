<?php

use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// LOGIN PHÍA ADMIN

// Tài khoản người sử dụng
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'user',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'UserController@index');
        $router->get('getPaginate', 'UserController@getPaginate');
        $router->post('create', 'UserController@create');
        $router->get('show/{id}', 'UserController@show');
        $router->post('update/{id}', 'UserController@update');
        $router->delete('remove/{id}', 'UserController@remove');
        $router->post('searchAll', 'UserController@searchAll');
    });
});

// NEWS
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'category',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'CategoryController@index');
        $router->post('create', 'CategoryController@create');
        $router->get('show/{id}', 'CategoryController@show');
        $router->post('update/{id}', 'CategoryController@update');
        $router->delete('remove/{id}', 'CategoryController@remove');
        $router->post('searchAll', 'CategoryController@searchAll');
        $router->post('upload', 'CategoryController@upload');
    });
});

// KIỂM DUYỆT POST
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'checkPost',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'CheckPostController@index');
        $router->delete('remove/{id}', 'CheckPostController@remove');
        $router->get('getListChecked', 'CheckPostController@getListChecked');
        $router->post('checked/{id}', 'CheckPostController@checked');
        $router->post('checkedAll/{id}/{checker}', 'CheckPostController@checkedAll');
        $router->post('searchAll', 'CheckPostController@searchAll');
    });
});

// POST
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'post',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'PostController@index');
        $router->post('create', 'PostController@create');
        $router->get('show/{id}', 'PostController@show');
        $router->post('update/{id}', 'PostController@update');
        $router->delete('remove/{id}', 'PostController@remove');
        $router->delete('removeImagesOld/{id}', 'PostController@RemoveImagesOld'); // test remove img old
        $router->post('searchAll', 'PostController@searchAll');
        $router->post('upload', 'PostController@upload');
    });
});

// POST TRANG CHỦ KO CẦN WA XÁC THỰC AIRLOCK
Route::group([
    'namespace' => 'Api\\CustomerApi',
    'prefix'    => 'post.customer',
], function (Registrar $router) {
    $router->post('create', 'PostCustomerController@create');
});

// NEWS
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'news',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'NewsController@index');
        $router->post('create', 'NewsController@create');
        $router->get('show/{id}', 'NewsController@show');
        $router->post('update/{id}', 'NewsController@update');
        $router->delete('remove/{id}', 'NewsController@remove');
        $router->post('searchAll', 'NewsController@searchAll');
        $router->post('upload', 'NewsController@upload');
    });
});

// AGENCY
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'agency',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'AgencyController@index');
        $router->post('create', 'AgencyController@create');
        $router->get('show/{id}', 'AgencyController@show');
        $router->post('update/{id}', 'AgencyController@update');
        $router->delete('remove/{id}', 'AgencyController@remove');
        $router->post('searchAll', 'AgencyController@searchAll');
        $router->post('upload', 'AgencyController@upload');
    });
});

// Additional Question
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'additionalQuestion',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'AdditionalQuestionController@index');
        $router->delete('remove/{id}', 'AdditionalQuestionController@remove');
        $router->post('searchAll', 'AdditionalQuestionController@searchAll');
    });
});

// Get Price List
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'getPriceList',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'GetPriceListController@index');
        $router->delete('remove/{id}', 'GetPriceListController@remove');
        $router->post('searchAll', 'GetPriceListController@searchAll');
    });
});

// Get Price List
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'watchLive',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'WatchLiveController@index');
        $router->delete('remove/{id}', 'WatchLiveController@remove');
        $router->post('searchAll', 'WatchLiveController@searchAll');
    });
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// AC OPTION LOCATION
// API dùng cho hộp kiểm chọn tỉnh thành, quận huyện, phường xã, đường phố, dự án
Route::get('province.list', 'ProvinceController@index');
Route::get('province.info/{id}', 'ProvinceController@show');
Route::get('province.getDistricts/{id}', 'DistrictController@getDistricts');
Route::get('district.bundle/{id}', 'DistrictController@bundle');
Route::get('district.geoData/{id}', 'DistrictController@geoData');

// IMAGE UPLOAD
Route::post('image.upload', 'ImageController@upload');
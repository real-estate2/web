<?php

use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeCustomerController@index')->name('home.get');






//demo
//ĐĂNG TIN NGAY
Route::get('/dang-tin', function () {
    return view('welcome');
})->name('post.form');

//demo



// RENT
Route::get('cho-thue', 'RentController@index')->name('rent.index');
Route::get('cho-thue/chi-tiet/{slug}.{id}.html', 'RentController@details')->name('rent.details');

// SELL
Route::get('can-ban', 'SellController@index')->name('sell.index');
Route::get('can-ban/chi-tiet/{slug}.{id}.html', 'SellController@details')->name('sell.details');

// PROJECT
Route::get('danh-muc-du-an', 'ProjectController@category')->name('project.category');
Route::get('loai-du-an/{id}.html', 'ProjectController@productCategory')->name('project.productCategory');
Route::get('du-an/{id}.html', 'ProjectController@index')->name('project.index');
Route::get('du-an/chi-tiet/{slug}.{id}.html', 'ProjectController@details')->name('project.details');

// ĐĂNG TIN NGAY
//Route::get('dang-tin', 'PostController@getForm')->name('form.get');
//Route::post('dang-tin', 'PostController@postForm')->name('form.post');

// IMAGE UPLOAD
Route::post('image.upload', 'ImageController@upload')->name('image.upload');

Route::get('image', 'HomeController@image')->name('image');




// API dùng cho hộp kiểm chọn tỉnh thành, quận huyện, phường xã, đường phố, dự án
Route::group([
    'namespace' => 'Autocomplete',
], function (Registrar $router) {
    $router->get('province.list', 'ProvinceController@index');
    $router->get('province.info/{id}', 'ProvinceController@show');
    $router->get('province.getDistricts/{id}', 'DistrictController@getDistricts');
    $router->get('district.bundle/{id}', 'DistrictController@bundle');
    $router->get('district.geoData/{id}', 'DistrictController@geoData');

    $router->get('district.getWards/{id}', 'WardController@getWards');
    $router->get('district.getStreets/{id}', 'StreetController@getStreets');
});

// Đăng kí xem trực tiếp - nhận bảng giá - hỏi thêm thông tin
Route::group([
    'namespace' => 'Customer',
], function (Registrar $router) {
    $router->post('additionalQuestion/{id}', 'AdditionalQuestionController@PostForm')->name('additionalQuestion.post');
    $router->post('getPriceList/{id}', 'GetPriceListController@PostForm')->name('getPriceList.post');
    $router->post('watchLive/{id}', 'WatchLiveController@PostForm')->name('watchLive.post');
});



// Tìm kiếm
Route::get('tim-kiem', 'SearchController@search')->name('search');
//Route::get('tim-kiem', 'SearchController@search2')->name('search');



// LOGIN
Route::get('dang-nhap', 'LoginController@getLogin')->name('login.get');
Route::post('dang-nhap', 'LoginController@postLogin')->name('login.post');

// Register
Route::get('dang-ki', 'LoginController@getRegister')->name('register.get');
Route::post('dang-ki', 'LoginController@postRegister')->name('register.post');

// Làm mới lại Captcha
Route::get('/refresh-captcha', 'LoginController@refresh')->name('refresh.captcha');

// Reset Password
Route::get('khoi-phuc-mat-khau', 'ResetPasswordController@getForm')->name('get.password.reset');
Route::post('khoi-phuc-mat-khau', 'ResetPasswordController@sendCode');

Route::get('mat-khau/cap-nhat', 'ResetPasswordController@resetPassword')->name('update.password');
Route::post('mat-khau/cap-nhat', 'ResetPasswordController@updatePassword')->name('update.password');

// LOGOUT
Route::get('dang-xuat', 'LoginController@logout')->name('logout.get');



// LIÊN HỆ
Route::get('contact', 'ContactController@index')->name('contact');


// Tin tức
Route::get('tin-tuc', 'NewsController@index')->name('news.index');
Route::get('tin-tuc/{slug}.{id}.html', 'NewsController@detail')->name('news.detail');

// Đại lý
Route::get('dai-ly', 'AgencyController@index')->name('agency.index');
Route::get('dai-ly/{slug}.{id}.html', 'AgencyController@detail')->name('agency.detail');

// Logout VUEJS
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout');

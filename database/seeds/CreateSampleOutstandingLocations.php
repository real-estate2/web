<?php

use App\OutstandingLocation;
use Illuminate\Database\Seeder;

class CreateSampleOutstandingLocations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = file_get_contents(__DIR__ . '/data/outstanding_locations.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing outstanding locations ${idx}/${c}";

            if (empty($item['name'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            $item['remote_id'] = 0;
            unset($item['created_at']);
            unset($item['updated_at']);

            $location = OutstandingLocation::create($item);
            $location->save();
        }

        echo PHP_EOL;
    }
}
<?php

use Illuminate\Database\Seeder;
use App\User;
use Carbon\Carbon;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            // Thông tin đăng nhập
            'username'   => 'test',
            'password'   => bcrypt('123456'),
            'last_name'  => 'Khách Hàng',
            'first_name' => 'Test',

            'email'                      => 'test@gmail.vn',
            'email_verified_at'          => Carbon::now(),
            'mobile'                     => '0123456789',
            'mobile_verified_at'         => Carbon::now(),

            // Thông tin cá nhân
            'sex'                        => 1,

            // Quyền hạn
            'post_privileged'            => true,
            'post_privileged_granted_at' => Carbon::now(),

        ]);

        User::create([
            // Thông tin đăng nhập
            'username'   => 'giautran',
            'password'   => bcrypt('123456'),
            'last_name'  => 'Trần Văn',
            'first_name' => 'Giàu',

            'email'                      => 'giautran@gmail.com',
            'email_verified_at'          => Carbon::now(),
            'mobile'                     => '0937519106',
            'mobile_verified_at'         => Carbon::now(),

            // Thông tin cá nhân
            'sex'                        => 1,

            // Quyền hạn
            'post_privileged'            => false,
            'post_privileged_granted_at' => Carbon::now(),

        ]);
    }
}

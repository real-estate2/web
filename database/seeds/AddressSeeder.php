<?php

use Illuminate\Database\Seeder;

class AddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::beginTransaction();
        $this->seedProvinces();
        $this->seedDistricts();
        $this->seedWards();
        $this->seedStreets();
        DB::commit();
    }

    private function seedProvinces()
    {
        $provinces = json_decode(file_get_contents(__DIR__.'/data/ac_provinces.json'), 1);

        $this->command->getOutput()->text('Provisioning provinces');
        $this->command->getOutput()->progressStart(count($provinces));

        \App\Models\AC\Province::query()->delete();

        foreach ($provinces as $province) {
            \App\Models\AC\Province::create($province);
            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }

    private function seedDistricts()
    {
        $districts = json_decode(file_get_contents(__DIR__.'/data/ac_districts.json'), 1);

        $this->command->getOutput()->text('Provisioning districts');
        $this->command->getOutput()->progressStart(count($districts));

        \App\Models\AC\District::query()->delete();

        foreach ($districts as $district) {
            \App\Models\AC\District::create($district);
            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }

    private function seedWards()
    {
        $wards = json_decode(file_get_contents(__DIR__.'/data/ac_wards.json'), 1);

        $this->command->getOutput()->text('Provisioning wards');
        $this->command->getOutput()->progressStart(count($wards));

        \App\Models\AC\Ward::query()->delete();

        foreach ($wards as $ward) {
            \App\Models\AC\Ward::create($ward);
            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }

    private function seedStreets()
    {
        $streets = json_decode(file_get_contents(__DIR__.'/data/ac_streets.json'), 1);

        $this->command->getOutput()->text('Provisioning streets');
        $this->command->getOutput()->progressStart(count($streets));

        \App\Models\AC\Street::query()->delete();

        foreach ($streets as $street) {
            \App\Models\AC\Street::create($street);
            $this->command->getOutput()->progressAdvance();
        }

        $this->command->getOutput()->progressFinish();
    }
}

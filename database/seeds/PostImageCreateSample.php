<?php

use Illuminate\Database\Seeder;
use App\PostImage;

class PostImageCreateSample extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = file_get_contents(__DIR__ . '/data/up/post_image.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing Post image ${idx}/${c}";

            if (empty($item['post_id'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['created_at']);
            unset($item['updated_at']);

            $post_image = PostImage::create($item);
            $post_image->save();
        }

        echo PHP_EOL;
    }
}

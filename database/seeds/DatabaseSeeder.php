<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AddressSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(ProjectCreateSample::class);
        $this->call(PostCreateSample::class);
        $this->call(PostImageCreateSample::class);
    }
}

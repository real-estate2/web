<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostCreateSample extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $content = file_get_contents(__DIR__ . '/data/up/post.json');
        $data = json_decode($content, true);

        $c = count($data);
        foreach ($data as $idx => $item) {
            $idx++;
            echo "\rProcessing Post ${idx}/${c}";

            if (empty($item['title'])) {
                echo 'Skipped' . PHP_EOL;
                continue;
            }

            unset($item['start_date']);
            unset($item['end_date']);
            unset($item['created_at']);
            unset($item['updated_at']);

            $post = Post::create($item);
            $post->save();
        }

        echo PHP_EOL;
    }
}

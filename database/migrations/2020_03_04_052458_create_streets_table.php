<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStreetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('streets', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('district_id')->comment('Mã quận huyện');
            $table->unsignedInteger('street_id')->unique()->comment('Mã đường phố');

            //$table->string('pre')->comment('Tiền tố cho tên, vd: Tỉnh lộ');
            $table->string('name')->comment('Tên đường');

            $table->timestamps();

            $table->index(['district_id', 'street_id']);

            $table->foreign('district_id')
                ->references('district_id')->on('districts')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('streets');
    }
}

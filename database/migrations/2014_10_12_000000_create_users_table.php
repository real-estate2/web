<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();

            // Thông tin đăng nhập
            $table->string('username')->unique()->comment('Tên tài khoản');
            $table->string('password')->comment('Mật khẩu, đã băm bằng bcrypt');
            $table->string('email')->unique()->comment('Mật khẩu, đã băm bằng bcrypt');
            $table->timestamp('email_verified_at')->nullable()->comment('Email xác minh thành công khi nào');

            $table->string('phone')->default('')->comment('Số điện thoại cố định');
            $table->string('mobile')->unique()->comment('Số điện thoại di động');
            $table->timestamp('mobile_verified_at')->nullable()->comment('Số điện thoại xác minh thành công khi nào');

            // Thông tin cá nhân
            $table->string('identity_number')->default('')->comment('CMND/Thẻ căn cước');
            $table->string('last_name')->comment('Họ');
            $table->string('first_name')->comment('Tên');
            $table->tinyInteger('sex')->default(1)->comment('Giới tính 0 = nữ, 1 = nam');
            $table->string('avatar')->default('')->comment('Hình đại diện, đường dẫn trên CDN, không kèm tên miền');

            $table->unsignedBigInteger('province')->default(0)->comment('Mã tỉnh');
            $table->unsignedBigInteger('district')->default(0)->comment('Mã quận huyện');
            $table->string('address')->default('')->comment('Địa chỉ liên hệ');

            // Thông tin thanh toán
            $table->unsignedBigInteger('primary_available_balance')->default(0)->comment('Số dư tài khoản chính');
            $table->unsignedBigInteger('promotion_available_balance1')->default(50000)->comment('Số dư tài khoản KM1');
            $table->unsignedBigInteger('promotion_available_balance2')->default(0)->comment('Số dư tài khoản KM2');

            // Quyền hạn
            $table->boolean('post_privileged')->default(false)->comment('Đăng bài không cần qua bước xét duyệt');
            $table->timestamp('post_privileged_granted_at')->nullable()->comment('Cấp quyền đăng không xét duyệt khi nào');
            $table->boolean('active')->default(true)->comment('Tình trạng khóa tài khoản');

            // Tạo code để reset password
            $table->string('code')->nullable()->index();
            $table->timestamp('time_code')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProvincesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('provinces', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('province_id')->unique()->comment('Mã tỉnh thành');
            $table->string('name')->comment('Tên tỉnh thành');

            $table->longText('polygon')->nullable()->comment('Đường biên vùng bản đồ; ranh giới tỉnh');
            //$table->polygon('polygon')->nullable()->comment('Đường biên vùng bản đồ; ranh giới tỉnh');
            $table->decimal('longitude', 10, 7)->nullable()->comment('Kinh độ, điểm căn giữa');
            $table->decimal('latitude', 10, 7)->nullable()->comment('Vĩ độ, điểm căn giữa');

            $table->index('province_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('provinces');
    }
}

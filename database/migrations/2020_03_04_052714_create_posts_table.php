<?php

use App\PostStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();

            // Thông tin cơ bản
            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');
            $table->string('title', 200)->comment('Tiêu đề bài viết');
            $table->decimal('price')->default(0)->comment('Giá bán, 0 = thỏa thuận');
            $table->unsignedInteger('price_unit')->comment('Đơn vị giá bán');
            $table->unsignedBigInteger('price_total')->default(0)->comment('Giá bán theo đơn vị bán');
            $table->unsignedInteger('area')->comment('Diện tích, 0 = N/a');
            $table->string('area_unit')->default('m2')->comment('Đơn vị diện tích bán');
            $table->string('address')->comment('Địa chỉ BĐS');
            $table->string('short_address')->default('')->comment('Địa chỉ ngắn, quận huyện + tỉnh');
            $table->text('description')->comment('Mô tả thêm');

            // Thông tin khác
            $table->unsignedTinyInteger('main_direction')->default(0)->comment('Hướng nhà');
            $table->unsignedTinyInteger('nbr_bed_room')->default(0)->comment('Số phòng ngủ');
            $table->unsignedTinyInteger('nbr_rest_room')->default(0)->comment('Số phòng về sinh');
            $table->unsignedTinyInteger('nbr_floor')->default(0)->comment('Số tầng');
            $table->unsignedTinyInteger('front_street_width')->default(0)->comment('Mặt tiền');
            $table->unsignedTinyInteger('behind_street_width')->default(0)->comment('Đường vào');

            // Bản đồ
            //$table->geometry('geopoint')->comment('Địa điểm trên bản đồ');
            $table->decimal('longitude', 10, 7)->nullable()->comment('Kinh độ');
            $table->decimal('latitude', 10, 7)->nullable()->comment('Vĩ độ');

            // Hình ảnh, video và vị trí
            $table->boolean('has_image')->default(false)->comment('Bài đăng có hình');
            $table->boolean('has_image_360')->default(false)->comment('Bài đăng có hình 360');
            $table->string('thumbnails')->default('')->comment('Hình đại diện');
            $table->string('youtube')->default('')->comment('Link video trên youtube');

            // Liên hệ
            $table->string('contact_name')->default('')->comment('Họ tên người liên hệ');
            $table->string('contact_address')->default('')->comment('Địa chỉ liên hệ');
            $table->string('contact_mobile')->default('')->comment('SĐT liên hệ');
            $table->string('contact_email')->default('')->comment('Email liên hệ');

            // Thông tin người đăng, người duyệt
            $table->unsignedBigInteger('author')->default(0)->comment('Người đăng');
            $table->unsignedBigInteger('approved_by')->nullable()->comment('Người phê duyệt');
            $table->timestamp('approved_at')->nullable()->comment('Thời gian duyệt');
            $table->unsignedBigInteger('rejected_by')->nullable()->comment('Người từ chối');
            $table->timestamp('rejected_at')->nullable()->comment('Thời từ chối');

            // Thông tin bổ sung, bổ trợ tìm kiếm
            $table->unsignedInteger('province_id')->nullable()->comment('Mã tỉnh');
            $table->unsignedInteger('district_id')->nullable()->comment('Mã quận huyện');
            $table->unsignedInteger('ward_id')->nullable()->comment('Mã phường xã');
            $table->unsignedInteger('street_id')->nullable()->comment('Mã đường phố');
            $table->unsignedBigInteger('project_id')->nullable()->comment('Mã dự án');

            $table->unsignedInteger('product_type')->comment('Loại hình mua bán, cho thuê');
            $table->unsignedInteger('product_category')->comment('Danh mục con');
            $table->unsignedInteger('post_type')->comment('Loại bài viết vip 0 1 2...');
            $table->date('start_date')->nullable()->comment('Ngày bắt đầu');
            $table->date('end_date')->nullable()->comment('Ngày kết thúc');
            $table->timestamp('pending_delete')->nullable()->comment('Thời gian xóa vĩnh viễn');
            $table->unsignedTinyInteger('status')->default(PostStatus::PENDING)->comment('Trang thái bài đăng');

            $table->timestamps();

            $table->foreign('author')
                ->references('id')->on('users')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('province_id')
                ->references('province_id')->on('provinces')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('district_id')
                ->references('district_id')->on('districts')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('ward_id')
                ->references('ward_id')->on('wards')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->foreign('street_id')
                ->references('street_id')->on('streets')
                ->onUpdate('restrict')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}

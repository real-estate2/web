<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wards', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('district_id')->comment('Mã quận huyện');
            $table->unsignedInteger('ward_id')->unique()->comment('Mã phường xã');

            //$table->string('pre')->comment('Tiền tố cho tên, vd: Xã');
            $table->string('name')->comment('Tên phường xã');
            $table->string('full_address')->default('')->comment('Tên phường xã + tên quận huyện + tên tỉnh');

            $table->timestamps();

            $table->index(['district_id', 'ward_id']);

            $table->foreign('district_id')
                ->references('district_id')->on('districts')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wards');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutstandingLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outstanding_locations', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('remote_id');
            $table->string('name');
            $table->string('type');

            $table->multiPolygon('geometry')->nullable();
            $table->decimal('longitude', 10, 7)->comment('Kinh độ');
            $table->decimal('latitude', 10, 7)->comment('Vĩ độ');

            $table->integer('zoom_level')->default(15)->comment('Mức thu phóng bản đồ');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outstanding_locations');
    }
}

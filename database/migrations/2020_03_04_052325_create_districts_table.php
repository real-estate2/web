<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('districts', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('province_id')->comment('Mã tỉnh thành');

            $table->unsignedInteger('district_id')->unique()->comment('Mã quận huyện');

            $table->string('pre')->comment('Tiền tố cho tên, vd: Quận');
            $table->string('name')->comment('Tên quận huyện');
            $table->string('full_address')->default('')->comment('Tên huyện + tên tỉnh');
            $table->string('keywords')->default('')->comment('Từ khóa SEO');

            $table->text('polygon')->comment('Đường biên vùng bản đồ; ranh giới quận / huyện');
            //$table->polygon('polygon')->comment('Đường biên vùng bản đồ; ranh giới quận / huyện');
            $table->decimal('longitude', 10, 7)->nullable()->comment('Kinh độ, điểm căn giữa');
            $table->decimal('latitude', 10, 7)->nullable()->comment('Vĩ độ, điểm căn giữa');

            $table->foreign('province_id')
                ->references('province_id')->on('provinces')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('districts');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdditionalQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_questions', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('post_id')->comment('Mã bài đăng, khóa ngoại');

            // Liên hệ
            $table->string('contact_name')->default('')->comment('Họ tên người liên hệ');
            $table->string('contact_address')->default('')->comment('Địa chỉ liên hệ');
            $table->string('contact_mobile')->default('')->comment('SĐT liên hệ');
            $table->string('contact_email')->default('')->comment('Email liên hệ');
            $table->tinyInteger('sex')->default(1)->comment('Giới tính 0 = nữ, 1 = nam');

            $table->text('question')->comment('Lời nhắn');

            $table->index(['post_id']);

            $table->foreign('post_id')
                ->references('id')->on('posts')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('additional_questions');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->id();

            $table->unsignedInteger('district_id')->comment('Mã quận huyện');

            $table->string('name', 100)->comment('Tên danh mục dự án');
            $table->text('description')->comment('Mô tả thêm');


            $table->string('thumbnails')->default('')->comment('Hình đại diện');

            $table->index(['district_id']);

            $table->foreign('district_id')
                ->references('district_id')->on('districts')
                ->onUpdate('restrict')
                ->onDelete('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}

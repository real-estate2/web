<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->id();

            $table->string('slug', 200)->comment('Đường dẫn thân thiện, SEO URL');

            $table->string('contact_name')->default('')->comment('Họ tên người đứng đầu đại lý');
            $table->string('thumbnails')->default('')->comment('Hình đại diện');
            $table->string('position')->default('')->comment('chức vụ');
            $table->string('contact_mobile')->default('')->comment('SĐT liên hệ');
            $table->string('contact_email')->default('')->comment('Email liên hệ');
            $table->string('company')->default('')->comment('Tên công ty');

            $table->longText('experience')->comment('Kinh nghiệm làm việc');
            $table->longText('introduce')->comment('Giới thiệu bản thân');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencies');
    }
}

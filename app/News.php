<?php

namespace App;

use App\Presenters\NewsPresenter;
use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\HasPresenter;

class News extends Model implements HasPresenter
{
    use DataTablePaginate;

    protected $fillable = [
        'title',
        'slug',
        'summary',
        'content',
        'content_type',
        'category_id',
        'thumbnails',
        'source',
        'start_date',
        'end_date',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'title',
        'slug',
        'summary',
        'content',
        'content_type',
        'category_id',
        'thumbnails',
        'source',
        'start_date',
        'end_date',
        'pending_remove',
    ];

    public function getPresenterClass()
    {
        return NewsPresenter::class;
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use ScoutElastic\Searchable;

class MyModel extends Model
{
    use Searchable;

    /**
     * @var string
     */
    protected $indexConfigurator = MyIndexConfigurator::class;

    /**
     * @var array
     */
    protected $searchRules = [
        //
    ];

    /**
     * @var array
     */
    protected $mapping = [
        'analysis' => [
            'analyzer' => [
                'es_std' => [
                    'type' => 'standard'
                ]
            ]
        ]
    ];

    use \Laravel\Scout\Searchable;

    protected $table = 'posts';

    protected $fillable = [
        'title',
        'slug',
        'price',
        'price_unit',
        'price_total',
        'area',
        'area_unit',
        'address',
        'description',
        'main_direction',
        'nbr_bed_room',
        'nbr_rest_room',
        'nbr_floor',
        'front_street_width',
        'behind_street_width',
        'longitude',
        'latitude',
        'has_image',
        'has_image_360',
        'thumbnails',
        'youtube',
        'contact_name',
        'contact_address',
        'contact_mobile',
        'contact_email',
        'author',
        'approved_by',
        'approved_at',
        'rejected_by',
        'rejected_at',
        'province_id',
        'district_id',
        'ward_id',
        'street_id',
        'project_id',
        'product_type',
        'post_type',
        'product_category',
        'status',
        'start_date',
        'end_date',
        'pending_remove',
    ];
}
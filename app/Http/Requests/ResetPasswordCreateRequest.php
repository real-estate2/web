<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResetPasswordCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password_reset' => 'required| min:6 | max:20',
            're_password_reset' => 'required|same:password_reset',
        ];
    }

    public function messages()
    {
        return [
            'password_reset.required' => 'Vui lòng nhập password',
            'password_reset.min' => 'Mật khẩu ít nhất 6 kí tự',

            're_password_reset.required' => 'Mật khẩu nhập lại không để trống',
            're_password_reset.same' => 'Mật khẩu không giống nhau',
        ];
    }
}
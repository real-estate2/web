<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'username'                                      => 'required',
            'password'                                      => 'required',
            'last_name'                                     => 'required',
            'first_name'                                    => 'required',
            'email'                                         => 'required',
            'email_verified_at'                             => 'nullable',
            'mobile'                                        => 'required',
            'mobile_verified_at'                            => 'nullable',
            'sex'                                           => 'required',
            'post_privileged'                               => 'nullable',
            'post_privileged_granted_at'                    => 'nullable'
        ];
    }

    public function messages()
    {
        return [
            'username.required'                             => 'Bạn chưa nhập tài khoản',
            'password.required'                             => 'Bạn chưa nhập mật khẩu',
            'last_name.required'                            => 'Bạn chưa nhập họ',
            'first_name.required'                           => 'Bạn chưa nhập tên',
            'email.required'                                => 'Bạn chưa nhập email',
            'mobile.required'                               => 'Bạn chưa nhập số điện thoại',
            'sex.required'                                  => 'Bạn chưa nhập giới tính',
        ];
    }
}
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AgencyCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contact_name'                                  => 'required',
            'thumbnails'                                    => 'required',
            'position'                                      => 'required',
            'contact_mobile'                                => 'required',
            'contact_email'                                 => 'required',
            'company'                                       => 'required',
            'experience'                                    => 'required',
            'introduce'                                     => 'required',
        ];
    }

    public function messages()
    {
        return [
            'contact_name.required'                         => 'Bạn chưa nhập tên liên hệ',
            'thumbnails.required'                           => 'Bạn chưa nhập hình ảnh',
            'position.required'                             => 'Bạn chưa nhập chức vụ',
            'contact_mobile.required'                       => 'Bạn chưa nhập số điện thoại',
            'contact_email.required'                        => 'Bạn chưa nhập địa chỉ email',
            'company.required'                              => 'Bạn chưa nhập công ty',
            'experience.required'                           => 'Bạn chưa nhập kinh nghiệp làm việc',
            'introduce.required'                            => 'Bạn chưa nhập lời giới thiệu',
        ];
    }
}

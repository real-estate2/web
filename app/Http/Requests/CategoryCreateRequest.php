<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'district_id'                           => 'required',
            'name'                                  => 'required',
            'description'                           => 'required',
            'thumbnails'                            => 'required',
        ];
    }

    public function messages()
    {
        return [
            'district_id.required'                  => 'Bạn chưa nhập quận/huyện',
            'name.required'                         => 'Bạn chưa nhập tên danh mục',
            'description.required'                  => 'Bạn chưa nhập mô tả dự án',
            'thumbnails.required'                   => 'Bạn chưa nhập hình ảnh',
        ];
    }
}
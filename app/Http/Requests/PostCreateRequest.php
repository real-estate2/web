<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'                                         => 'required',
            'price'                                         => 'required',
            'price_unit'                                    => 'required',
//            'price_total'                                   => 'required',
            'area'                                          => 'required',
            'area_unit'                                     => 'required',
            'address'                                       => 'required',
            'description'                                   => 'required',
            'main_direction'                                => 'required',
            'nbr_bed_room'                                  => 'required',
            'nbr_rest_room'                                 => 'required',
            'nbr_floor'                                     => 'required',
            'front_street_width'                            => 'required',
            'behind_street_width'                           => 'required',
            'longitude'                                     => 'required',
            'latitude'                                      => 'required',
//            'has_image'                                     => 'required',
//            'has_image_360'                                 => 'required',
            'thumbnails'                                    => 'required',

            // Thư viện ảnh
            'images.*.filepath'                             => 'required',
            'images.*.type'                                 => 'required',
            'images.*.sort_order'                           => 'required|numeric',
            'images.*.uploaded_data.*.ranting'              => 'required',
            'images.*.uploaded_data.*.filename'             => 'required',
            'images.*.uploaded_data.*.width'                => 'required|numeric',
            'images.*.uploaded_data.*.height'               => 'required|numeric',

            'youtube'                                       => 'required',
            'contact_name'                                  => 'required',
            'contact_address'                               => 'required',
            'contact_mobile'                                => 'required',
            'contact_email'                                 => 'required',
//            'author'                                        => 'required',
//            'approved_by'                                   => 'required',
//            'approved_at'                                   => 'required',
//            'rejected_by'                                   => 'required',
//            'rejected_at'                                   => 'required',
            'province_id'                                   => 'required',
            'district_id'                                   => 'required',
            'ward_id'                                       => 'required',
            'street_id'                                     => 'required',
            'project_id'                                    => 'required',
            'product_type'                                  => 'required',
            'post_type'                                     => 'required',
            'product_category'                              => 'required',
            'start_date'                                    => 'required',
            'end_date'                                      => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required'                                => 'Bạn chưa nhập tiêu đề',
            'price.required'                                => 'Bạn chưa nhập giá bán , 0 = thỏa thuận',
            'price_unit.required'                           => 'Bạn chưa nhập đơn vị giá bán',
//            'price_total.required'                          => 'Bạn chưa nhập giá bán theo đơn vị bán',
            'area.required'                                 => 'Bạn chưa nhập diện tích, 0 = N/a',
            'area_unit.required'                            => 'Bạn chưa nhập đơn vị diện tích bán',
            'address.required'                              => 'Bạn chưa nhập địa chỉ BĐS',
            'description.required'                          => 'Bạn chưa nhập mô tả thêm',
            'main_direction.required'                       => 'Bạn chưa nhập hướng nhà',
            'nbr_bed_room.required'                         => 'Bạn chưa nhập số phòng ngủ',
            'nbr_rest_room.required'                        => 'Bạn chưa nhập số phòng về sinh',
            'nbr_floor.required'                            => 'Bạn chưa nhập số tầng',
            'front_street_width.required'                   => 'Bạn chưa nhập mặt tiền',
            'behind_street_width.required'                  => 'Bạn chưa nhập đường vào',
            'longitude.required'                            => 'Bạn chưa nhập kinh độ',
            'latitude.required'                             => 'Bạn chưa nhập vĩ độ',
//            'has_image.required'                            => 'Bạn chưa nhập bài đăng có hình',
//            'has_image_360.required'                        => 'Bạn chưa nhập bài đăng có hình 360',
            'thumbnails.required'                           => 'Bạn chưa nhập hình đại diện',
            'youtube.required'                              => 'Bạn chưa nhập link video trên youtube',
            'contact_name.required'                         => 'Bạn chưa nhập họ tên người liên hệ',
            'contact_address.required'                      => 'Bạn chưa nhập địa chỉ liên hệ',
            'contact_mobile.required'                       => 'Bạn chưa nhập sđt liên hệ',
            'contact_email.required'                        => 'Bạn chưa nhập email liên hệ',
//            'author.required'                               => 'Bạn chưa nhập người đăng',
//            'approved_by.required'                          => 'Bạn chưa nhập người phê duyệt',
//            'approved_at.required'                          => 'Bạn chưa nhập thời gian duyệt',
//            'rejected_by.required'                          => 'Bạn chưa nhập người từ chối',
//            'rejected_at.required'                          => 'Bạn chưa nhập thời từ chối',

            'province_id.required'                          => 'Bạn chưa nhập mã tỉnh',
            'district_id.required'                          => 'Bạn chưa nhập mã quận huyện',
            'ward_id.required'                              => 'Bạn chưa nhập mã phường xã',
            'street_id.required'                            => 'Bạn chưa nhập mã đường phố',
            'project_id.required'                           => 'Bạn chưa nhập mã dự án',

            'product_type.required'                         => 'Bạn chưa nhập loại hình mua bán, cho thuê',
            'post_type.required'                            => 'Bạn chưa nhập loại bài viết vip 0 1 2...',
            'product_category.required'                     => 'Bạn chưa nhập danh mục con...',
            'start_date.required'                           => 'Bạn chưa nhập ngày bắt đầuc đăng tin',
            'end_date.required'                             => 'Bạn chưa nhập ngày kết thúc đăng tin',
        ];
    }
}
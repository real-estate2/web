<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewsCategoryCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'                           => 'required',
            'name'                                  => 'required|max:100'
        ];
    }

    public function messages()
    {
        return [
            'category_id.required'                  => 'Bạn chưa nhập mã danh mục',
            'name.required'                         => 'Bạn chưa nhập tên danh mục',
            'name.max'                              => 'Tên danh mục không được quá 100 kí tự'
        ];
    }
}

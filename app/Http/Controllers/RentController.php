<?php

namespace App\Http\Controllers;

use App\Post;
use App\OutstandingLocation;
use Illuminate\Http\Request;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;
use Auth;

class RentController extends Controller
{
    public function index()
    {
        $post = Post::query()
        ->where('product_type', '=', 49)
        ->where('status', '!=', 0)
        ->orderBy('post_type')
        ->orderByDesc('created_at')
        ->get(); // 49: loại cho thuê || 38: loại bán
        return view('post.rent.index', [
            'post' => $post
        ]);
    }

    public function details($slug, $id)
    {
        $post = Post::query()->findOrFail($id);

        $post_order = Post::query()
            ->where('product_type', '=', 49)
            ->where('status', '!=', 0)
            ->orderBy('post_type')
            ->take(4)
            ->get(); // 49: loại cho thuê || 38: loại bán

        $post->load('images');

        $list_image = [];
        $list_image_360 = [];

        foreach ($post->images as $item) {
            if ($item->type == 'image_360') {
                $list_image_360[] = $item;
            } else {
                $list_image[] = $item;
            }
        }

        $query = OutstandingLocation::query();
        $query->distance($post->latitude, $post->longitude);
        $query->orderBy('distance', 'ASC')->get();
        $query->limit(5);

        return view('post.rent.detail', [
            'item' => $post,
            'list_image' => $list_image,
            'order' => $post_order,

            'data' => AutoPresenter::decorate($post),
            'nearBy' => $query->get(),
        ]);
    }
}

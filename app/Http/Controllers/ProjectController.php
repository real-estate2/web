<?php

namespace App\Http\Controllers;

use App\Category;
use App\OutstandingLocation;
use App\Post;
use Illuminate\Http\Request;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;
use App\District;

class ProjectController extends Controller
{
    public function category()
    {
        $category = Category::query()->get();

        return view('post.project.category', [
            'category' => $category
        ]);
    }

    public function productCategory($product_category)
    {
        $post = Post::query()
            // ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->where('product_category', '=', $product_category)
            ->with('projects')
            ->get();

        return view('post.project.productCategory', [
            'post' => $post
        ]);
    }

    public function index($project_id)
    {
        $post = Post::query()
            ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->where('project_id', '=', $project_id)
            ->with('projects')
            ->orderBy('post_type')
            ->get();

         $project = Category::query()->where('id', '=', $project_id)->firstOrFail();

        $district = null;
        if($project) {
            $district = District::query()->where('district_id', '=', $project->district_id)->firstOrFail();
        }
        
        // dd($project);

        return view('post.project.index', [
            'post' => $post,
            'district' => $district,
            'project' => $project
        ]);
    }

    public function details($slug, $id)
    {
        $post = Post::query()->findOrFail($id);

        $post_order = Post::query()
            ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->with('projects')
            ->take(4)
            ->get();

        $post->load('images');

        $list_image = [];
        $list_image_360 = [];

        foreach ($post->images as $item) {
            if ($item->type == 'image_360') {
                $list_image_360[] = $item;
            } else {
                $list_image[] = $item;
            }
        }

        $query = OutstandingLocation::query();
        $query->distance($post->latitude, $post->longitude);
        $query->orderBy('distance', 'ASC')->get();
        $query->limit(5);

        return view('post.project.detail', [
            'item' => $post,
            'list_image' => $list_image,
            'order' => $post_order,

            'data' => AutoPresenter::decorate($post),
            'nearBy' => $query->get(),
        ]);
    }
}

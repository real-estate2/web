<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index()
    {
        $data['news'] = News::query()->take(12)->get();
        return view('news.index', $data);
    }

    public function detail($slug, $id)
    {
        $data = News::findOrFail($id);
        $similar_data =  News::where('id', '!=' , $id)->take(4)->get();
        return view('news.detail', [
            'item' => $data,
            'similar_data' => $similar_data
        ]);
    }
}

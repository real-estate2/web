<?php

namespace App\Http\Controllers;

use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ImageController extends AbstractApiController
{
    public function upload(Request $request)
    {
        if (!$request->hasFile('file')) {
            return response()->json(["message" => "Chọn ảnh thất bại"]);
        }

        $bucket_prefix = config('filesystems.bucket_prefix', '');

        $slugify = new Slugify();
        $bucket_path = 'images/post/' . $bucket_prefix . now()->toDateString();
        $file = $request->file('file');
        $fileOriginalName = $file->getClientOriginalName();
        $fileExtension = $file->getClientOriginalExtension();
        $fileName = str_replace('.' . $fileExtension, '', $fileOriginalName);
        $fileName = $slugify->slugify($fileName);

        // Bug fix request timeout
        set_time_limit(90);
        ini_set('memory_limit', '256M');

        $_t = substr($fileOriginalName, 0, 4);
        $is360 = $_t === '360_' || $_t === 'PANO';

        if (!$is360) {
            $expect_size = [
                'o' => 1080,
                'xs' => 360,
            ];
        } else {
            $expect_size = [
                'o' => 5000,
                'xs' => 360,
            ];
        }

        $result = [
            'filepath' => $bucket_path,
            'type' => !$is360 ? 'image' : 'image_360',
            'files' => [],
        ];

        foreach ($expect_size as $ranting => $size) {
            $img = Image::make($file);
            $img->resize($size, null, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $newFilename = $fileName . '_' . time() . '_' . $ranting . '.jpg';

            $img->save('images/post/' . $newFilename, 80, 'jpg');

            array_push($result['files'], [
                'filename' => $newFilename,
                'ranting' => $ranting,
                'width' => $img->getWidth(),
                'height' => $img->getHeight(),
            ]);

            $img->destroy();
        }

        $arrrs = [];
        array_push($arrrs, $result);
        session(['images' => $this->item($result)]);
//        return response()->json(["images" => $result]);
        return $this->item($result);
    }
    /**
     * Upload hình vào bucket tạm và tạo các hình thu nhỏ với kích thức khác nhau
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
//    public function upload(Request $request)
//    {
//        if ($request->TotalImages > 0) {
//            for ($x = 0; $x < $request->TotalImages; $x++) {
//                if (!$request->hasFile('images' . $x)) {
//                    return response()->json('Xin lỗi đã vi phạm chuẩn IMG');
//                }
//
//                $bucket_prefix = config('filesystems.bucket_prefix', '');
//
//                $slugify = new Slugify();
//                $bucket_path = 'images/post/' . $bucket_prefix . now()->toDateString();
//                $file = $request->file('images' . $x);
//                $fileOriginalName = $file->getClientOriginalName();
//                $fileExtension = $file->getClientOriginalExtension();
//
//                $fileName = str_replace('.' . $fileExtension, '', $fileOriginalName);
//                $fileName = $slugify->slugify($fileName);
//                // Bug fix request timeout
//                set_time_limit(90);
//                ini_set('memory_limit', '256M');
//
//                $_t = substr($fileOriginalName, 0, 4);
//                $is360 = $_t === '360_' || $_t === 'PANO';
//
//                if (!$is360) {
//                    $expect_size = [
//                        'o' => 1080,
//                        'xs' => 360,
//                    ];
//                } else {
//                    $expect_size = [
//                        'o' => 5000,
//                        'xs' => 360,
//                    ];
//                }
//
//                $result [] = [
//                    'filepath' => $bucket_path,
//                    'type' => !$is360 ? 'image' : 'image_360',
//                    'files' => [],
//                ];
//
//                foreach ($expect_size as $ranting => $size) {
//                    $img = Image::make($file);
//                    $img->resize($size, null, function ($constraint) {
//                        $constraint->aspectRatio();
//                        $constraint->upsize();
//                    });
//
//                    $newFilename = $fileName . '_' . time() . '_' . $ranting . '.jpg';
//
//                    $img->save('images/post/' . $newFilename, 80, 'jpg');
//
//                    array_push($result [$x]['files'], [
//                        'filename' => $newFilename,
//                        'ranting' => $ranting,
//                        'width' => $img->getWidth(),
//                        'height' => $img->getHeight(),
//                    ]);
//                    $img->destroy();
//                }
//            }
//            session(['images' => $result]);
//            return response()->json(["message" => "Chọn ảnh hành công", "filename" => $newFilename, "arrimg" => $result]);
//        } else {
//            return response()->json(["message" => "Chọn ảnh thất bại"]);
//        }
//    }
}

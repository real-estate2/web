<?php

namespace App\Http\Controllers\Api\CustomerApi;
use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\PostCreateRequest;
use App\Post;
use App\PostImage;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PostCustomerController extends AbstractApiController
{
    public function list()
    {
        return Post::all();
    }
    public function index(Request $request)
    {
        $post = Post::query()
            ->select([
                'id',
                'title',
                'slug',
                'price',
                'price_unit',
                'price_total',
                'area',
                'area_unit',
                'address',
                'description',
                'main_direction',
                'nbr_bed_room',
                'nbr_rest_room',
                'nbr_floor',
                'front_street_width',
                'behind_street_width',
                'longitude',
                'latitude',
                'has_image',
                'has_image_360',
                'thumbnails',
                'youtube',
                'contact_name',
                'contact_address',
                'contact_mobile',
                'contact_email',
                'author',
                'approved_by',
                'approved_at',
                'rejected_by',
                'rejected_at',
                'province_id',
                'district_id',
                'ward_id',
                'street_id',
                'project_id',
                'product_type',
                'post_type',
                'product_category',
                'status',
                'start_date',
                'end_date',
            ])
            ->where('status', '!=', 0)
            ->DataTablePaginate($request);

        return $this->item($post);
    }

    public function create(PostCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['title']                               = $validatedData['title'];
        $payload['slug']                                = $slugify->slugify($validatedData['title']);

        $payload['price']                               = $validatedData['price'];
        $payload['price_unit']                          = $validatedData['price_unit'];
        $payload['price_total']                         = 0;

        $payload['area']                                = $validatedData['area'];
        $payload['area_unit']                           = $validatedData['area_unit'];
        $payload['address']                             = $validatedData['address'];
        $payload['description']                         = $validatedData['description'];
        $payload['main_direction']                      = $validatedData['main_direction'];
        $payload['nbr_bed_room']                        = $validatedData['nbr_bed_room'];
        $payload['nbr_rest_room']                       = $validatedData['nbr_rest_room'];
        $payload['nbr_floor']                           = $validatedData['nbr_floor'];
        $payload['front_street_width']                  = $validatedData['front_street_width'];
        $payload['behind_street_width']                 = $validatedData['behind_street_width'];

        // Hình ảnh
        $payload['images']                              = ! empty($validatedData['images']) ? $validatedData['images'] : [];

        // Bản đồ
        $payload['longitude']                           = $validatedData['longitude'];
        $payload['latitude']                            = $validatedData['latitude'];

        $payload['has_image']                           = false;
        $payload['has_image_360']                       = false;

        $payload['thumbnails']                          = $validatedData['thumbnails'];
        $payload['youtube']                             = $validatedData['youtube'];
        $payload['contact_name']                        = $validatedData['contact_name'];
        $payload['contact_address']                     = $validatedData['contact_address'];
        $payload['contact_mobile']                      = $validatedData['contact_mobile'];
        $payload['contact_email']                       = $validatedData['contact_email'];

//        $payload['author']                              = $validatedData['author'];
//        $payload['approved_by']                         = $validatedData['approved_by'];
//        $payload['approved_at']                         = $validatedData['approved_at'];
//        $payload['rejected_by']                         = $validatedData['rejected_by'];
//        $payload['rejected_at']                         = $validatedData['rejected_at'];
        $payload['author']                              = Auth::id();
        $payload['approved_by']                         = 1;
        $payload['approved_at']                         = '2020-02-25';
        $payload['rejected_by']                         = 1;
        $payload['rejected_at']                         = '2020-02-26';

        $payload['province_id']                         = $validatedData['province_id'];
        $payload['district_id']                         = $validatedData['district_id'];
        $payload['ward_id']                             = $validatedData['ward_id'];
        $payload['street_id']                           = $validatedData['street_id'];

        $payload['project_id']                          = $validatedData['project_id'];
        $payload['product_type']                        = $validatedData['product_type'];
        $payload['post_type']                           = $validatedData['post_type'];
        $payload['product_category']                    = $validatedData['product_category'];

        $payload['start_date']                          = $validatedData['start_date'];
        $payload['end_date']                            = $validatedData['end_date'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['title'])) {
            $this->setMessage('Đã tồn tại tiêu đề');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $post = Post::create($payload);
        DB::beginTransaction();

        if (! empty($payload['images'])) {
//                foreach ($payload['images'] as $image_idx => $image) {
//                    $currentBucketPath = $image['filepath'];
//
//                    foreach ($image['uploaded_data'] as $thumb_index => $targetFile) {
//                        Storage::disk('minio')->copy(
//                            $currentBucketPath.'/'.$targetFile['filename'],
//                            $newBucketPath.'/'.$targetFile['filename']
//                        );
//
//                        $payload['images'][$image_idx]['filepath'] = $newBucketPath;
//                    }
//                }

            // Cập nhật bucket path
            $post->addImages($payload['images']);
        }

        try {
            $post->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm bài đăng thành công!');
            $this->setStatusCode(200);
            $this->setData($post);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
//        return Post::query()->findOrFail($id);
        $images = PostImage::query()->where('post_id', '=', $id)->get();
        $post = Post::query()->findOrFail($id);

        $result = [
            'images' => $images,
            'post'     => $post,
        ];

        return $this->item($result);
    }

    public function update(PostCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $post = Post::query()->findOrFail($id);
        if (!$post) {
            $this->setMessage('Không có bài đăng này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $post->title                                = $validatedData['title'];
                $post->slug                                 = $slugify->slugify($validatedData['title']);
                $post->price                                = $validatedData['price'];
                $post->price_unit                           = $validatedData['price_unit'];
                $post->price_total                          = 0;
                $post->area                                 = $validatedData['area'];
                $post->area_unit                            = $validatedData['area_unit'];
                $post->address                              = $validatedData['address'];
                $post->description                          = $validatedData['description'];
                $post->main_direction                       = $validatedData['main_direction'];
                $post->nbr_bed_room                         = $validatedData['nbr_bed_room'];
                $post->nbr_rest_room                        = $validatedData['nbr_rest_room'];
                $post->nbr_floor                            = $validatedData['nbr_floor'];
                $post->front_street_width                   = $validatedData['front_street_width'];
                $post->behind_street_width                  = $validatedData['behind_street_width'];
                $post->longitude                            = $validatedData['longitude'];
                $post->latitude                             = $validatedData['latitude'];
                $post->has_image                            = false;
                $post->has_image_360                        = false;

//                $post->images                              = ! empty($validatedData['images']) ? $validatedData['images'] : [];

                $post->thumbnails                           = $validatedData['thumbnails'];
                $post->youtube                              = $validatedData['youtube'];
                $post->contact_name                         = $validatedData['contact_name'];
                $post->contact_address                      = $validatedData['contact_address'];
                $post->contact_mobile                       = $validatedData['contact_mobile'];
                $post->contact_email                        = $validatedData['contact_email'];

//                $post->author                               = Auth::id();
                $post->approved_by                          = Auth::id();
                $post->approved_at                          = Carbon::now();
                $post->rejected_by                          = Auth::id();
                $post->rejected_at                          = Carbon::now();

                $post->province_id                          = 3;
                $post->district_id                          = 159;
                $post->ward_id                              = 11855;
                $post->street_id                            = 278;
                $post->project_id                           = 0;

                $post->product_type                         = $validatedData['product_type'];
                $post->post_type                            = $validatedData['post_type'];
                $post->product_category                     = $validatedData['product_category'];
                $post->start_date                           = $validatedData['start_date'];
                $post->end_date                             = $validatedData['end_date'];
                // Cập nhật sort order
//                $school->sort_order = $validatedData['sort_order'];

                // Tạo và lưu
//                $post = Post::create($payload);
//                DB::beginTransaction();

                if (! empty($post->images)) {
                    // Cập nhật bucket path
                    $post->addImages($post->images);
                }

                $post->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($post);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Post::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function undoRemove($id)
    {
        $post = Post::query()->findOrFail($id);
        $deleted_time = $post->pending_remove;
        if (!$deleted_time) {
            $this->setMessage('Bài đăng không được đánh dấu chờ xóa');
            $this->setStatus(400);
            return $this->respond();
        }

        $time_deleted = Carbon::create($deleted_time);
        $time_now = Carbon::now();
        $time_diff = $time_deleted->diffInMinutes($time_now, false);

        // Không phép khôi phục sau 15p,
        if ($time_diff > 15) {
            $this->setMessage('Quá thời gian 15p cho phép khôi phục lại bài đăng đang chờ xóa [' . $time_diff . ']');
            $this->setStatusCode(400);
            return $this->respond();
        }

        DB::beginTransaction();
        $post->pending_remove = null;
        $post->save();
        DB::commit();

        return $this->noContent();
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($title)
    {
        $post = Post::query()->get();
        foreach ($post->pluck('title') as $item) {
            if ($title == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $post = Post::query()
            ->select([
                'id',
                'title',
                'slug',
                'price',
                'price_unit',
                'price_total',
                'area',
                'area_unit',
                'address',
                'description',
                'main_direction',
                'nbr_bed_room',
                'nbr_rest_room',
                'nbr_floor',
                'front_street_width',
                'behind_street_width',
                'longitude',
                'latitude',
                'has_image',
                'has_image_360',
                'thumbnails',
                'youtube',
                'contact_name',
                'contact_address',
                'contact_mobile',
                'contact_email',
                'author',
                'approved_by',
                'approved_at',
                'rejected_by',
                'rejected_at',
                'province_id',
                'district_id',
                'ward_id',
                'street_id',
                'project_id',
                'product_type',
                'post_type',
                'product_category',
                'status',
                'start_date',
                'end_date',
            ])
            ->where('status', '!=', 0)
            ->orWhere('title', 'LIKE', "%$search%")
            ->orWhere('price', 'LIKE', "%$search%")
            ->orWhere('address', 'LIKE', "%$search%")
            ->orWhere('area', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($post);
    }
}

<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\GetPriceList;
use App\Http\Controllers\AbstractApiController;

use Illuminate\Http\Request;

class GetPriceListController extends AbstractApiController
{
    public function index(Request $request)
    {
        $getPriceList = GetPriceList::query()
            ->select([
                'id',
                'post_id',
                'contact_name',
                'contact_address',
                'contact_mobile',
                'contact_email',
                'sex',
            ])
            ->with('posts')
            ->DataTablePaginate($request);

        return $this->item($getPriceList);
    }

    public function remove($id)
    {
        GetPriceList::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $getPriceList = GetPriceList::query()
            ->select([
                'id',
                'post_id',
                'contact_name',
                'contact_address',
                'contact_mobile',
                'contact_email',
                'sex',
                'question',
            ])
            ->with('posts')
            ->where('contact_name', 'LIKE', "%$search%")
            ->orWhere('contact_mobile', 'LIKE', "%$search%")
            ->orWhere('contact_email', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($getPriceList);
    }
}

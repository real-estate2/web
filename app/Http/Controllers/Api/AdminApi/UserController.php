<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;
use Illuminate\Http\Request;
use App\Http\Requests\UserCreateRequest;
use App\User;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;

class UserController extends AbstractApiController
{
    public function index()
    {
        return response()->json(User::with('staffs')->get(), 200);
    }

    public function getPaginate(Request $request)
    {
        $User = User::query()
            ->select([
                'id',
                'username',
                'password',
                'last_name',
                'first_name',
                'email',
                'mobile',
                'sex'
            ])
            ->DataTablePaginate($request);

        return $this->item($User);
    }

    public function create(UserCreateRequest $request)
    {
        $validatedData = $request->validated();

        $payload = [];

        $payload['username']                                = $validatedData['username'];
        $payload['password']                                = bcrypt($validatedData['password']);
        $payload['last_name']                               = $validatedData['last_name'];
        $payload['first_name']                              = $validatedData['first_name'];
        $payload['email']                                   = $validatedData['email'];
        $payload['email_verified_at']                       = Carbon::now();
        $payload['mobile']                                  = $validatedData['mobile'];
        $payload['mobile_verified_at']                      = Carbon::now();
        $payload['sex']                                     = $validatedData['sex'];
        $payload['post_privileged']                         = false;
        $payload['post_privileged_granted_at']              = Carbon::now();

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['username'])) {
            $this->setMessage('Đã tồn tại tên tài khoản');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu tài khoản
        $User = User::create($payload);
        DB::beginTransaction();

        try {
            $User->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm tài khoản thành công!');
            $this->setStatusCode(200);
            $this->setData($User);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return User::findOrFail($id);
    }

    public function update(UserCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $User = User::query()->findOrFail($id);
        if (! $User) {
            $this->setMessage('Không có tài khoản này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên tài khoản
                $User->username                                 = $validatedData['username'];
                $User->password                                 = bcrypt($validatedData['password']);
                $User->last_name                                = $validatedData['last_name'];
                $User->first_name                               = $validatedData['first_name'];
                $User->email                                    = $validatedData['email'];
                $User->email_verified_at                        = $validatedData['email_verified_at'];
                $User->mobile                                   = $validatedData['mobile'];
                $User->mobile_verified_at                       = $validatedData['mobile_verified_at'];
                $User->sex                                      = $validatedData['sex'];
                $User->post_privileged                          = $validatedData['post_privileged'];
                $User->post_privileged_granted_at               = $validatedData['post_privileged_granted_at'];

                $User->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($User);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        User::find($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($username)
    {
        $User = User::query()->get();
        foreach ($User->pluck('username') as $item) {
            if ($username == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $User = User::query()
            ->select([
                'id',
                'username',
                'password',
                'last_name',
                'first_name',
                'email',
                'mobile',
                'sex'
            ])
            ->where('username', 'LIKE', "%$search%")
            ->orWhere('last_name', 'LIKE', "%$search%")
            ->orWhere('first_name', 'LIKE', "%$search%")
            ->orWhere('email', 'LIKE', "%$search%")
            ->orWhere('mobile', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($User);
    }
}

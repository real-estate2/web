<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Category;
use App\Http\Controllers\AbstractApiController;
use App\Http\Requests\CategoryCreateRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends AbstractApiController
{
    public function index(Request $request)
    {
        $category = Category::query()
            ->select([
                'id',
                'district_id',
                'name',
                'description',
                'thumbnails',
            ])
            ->DataTablePaginate($request);

        return $this->item($category);
    }

    public function create(CategoryCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['district_id']                           = $validatedData['district_id'];
        $payload['description']                           = $validatedData['description'];
        $payload['name']                                  = $validatedData['name'];
        $payload['thumbnails']                            = $validatedData['thumbnails'];
        
        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên danh mục');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $category = Category::create($payload);
        DB::beginTransaction();

        try {
            $category->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm danh mục thành công!');
            $this->setStatusCode(200);
            $this->setData($category);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Category::query()->findOrFail($id);
    }

    public function update(CategoryCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $category = Category::query()->findOrFail($id);
        if (!$category) {
            $this->setMessage('Không có danh mục này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $category->district_id                            = $validatedData['district_id'];
                $category->description                            = $validatedData['description'];
                $category->name                                   = $validatedData['name'];
                $category->thumbnails                             = $validatedData['thumbnails'];

                $category->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($category);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Category::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $category = Category::query()->get();
        foreach ($category->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $category = Category::query()
            ->select([
                'id',
                'name',
                'description'
            ])
            ->where('name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($category);
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/categories'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}

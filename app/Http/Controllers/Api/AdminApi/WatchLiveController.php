<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\WatchLive;
use App\Http\Controllers\AbstractApiController;
use Illuminate\Http\Request;

class WatchLiveController extends AbstractApiController
{
    public function index(Request $request)
    {
        $watchLive = WatchLive::query()
            ->select([
                'id',
                'post_id',
                'contact_name',
                'contact_address',
                'contact_mobile',
                'contact_email',
                'sex',
                'live_date',
            ])
            ->with('posts')
            ->DataTablePaginate($request);

        return $this->item($watchLive);
    }

    public function remove($id)
    {
        WatchLive::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $watchLive = WatchLive::query()
            ->select([
                'id',
                'post_id',
                'contact_name',
                'contact_address',
                'contact_mobile',
                'contact_email',
                'sex',
                'question',
            ])
            ->with('posts')
            ->where('contact_name', 'LIKE', "%$search%")
            ->orWhere('contact_mobile', 'LIKE', "%$search%")
            ->orWhere('contact_email', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($watchLive);
    }
}

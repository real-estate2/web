<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\AdditionalQuestion;
use Illuminate\Http\Request;

class AdditionalQuestionController extends AbstractApiController
{
    public function index(Request $request)
    {
        $additionalQuestion = AdditionalQuestion::query()
            ->select([
                'id',
                'post_id',
                'contact_name',
                'contact_address',
                'contact_mobile',
                'contact_email',
                'sex',
                'question',
            ])
            ->with('posts')
            ->DataTablePaginate($request);

        return $this->item($additionalQuestion);
    }

    public function remove($id)
    {
        AdditionalQuestion::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $additionalQuestion = AdditionalQuestion::query()
            ->select([
                'id',
                'post_id',
                'contact_name',
                'contact_address',
                'contact_mobile',
                'contact_email',
                'sex',
                'question',
            ])
            ->with('posts')
            ->where('contact_name', 'LIKE', "%$search%")
            ->orWhere('contact_mobile', 'LIKE', "%$search%")
            ->orWhere('contact_email', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($additionalQuestion);
    }
}

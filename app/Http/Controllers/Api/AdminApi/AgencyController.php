<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;
use App\Http\Requests\AgencyCreateRequest;
use App\Agency;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AgencyController extends AbstractApiController
{
    public function index(Request $request)
    {
        $agency = Agency::query()
            ->select([
                'id',
                'contact_name',
                'thumbnails',
                'position',
                'contact_mobile',
                'contact_email',
                'company',
                'experience',
                'introduce',
            ])
            ->DataTablePaginate($request);

        return $this->item($agency);
    }

    public function create(AgencyCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['contact_name']                            = $validatedData['contact_name'];
        $payload['slug']                                    = $slugify->slugify($validatedData['contact_name']);
        $payload['thumbnails']                              = $validatedData['thumbnails'];
        $payload['position']                                = $validatedData['position'];
        $payload['contact_mobile']                          = $validatedData['contact_mobile'];
        $payload['contact_email']                           = $validatedData['contact_email'];
        $payload['company']                                 = $validatedData['company'];
        $payload['experience']                              = $validatedData['experience'];
        $payload['introduce']                               = $validatedData['introduce'];

        // Kiểm tra trùng tên
        if (!$this->checkDuplicateName($payload['contact_mobile'])) {
            $this->setMessage('Đã tồn tại số điện thoại');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu
        $agency = Agency::create($payload);
        DB::beginTransaction();

        try {
            $agency->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm đại lý thành công!');
            $this->setStatusCode(200);
            $this->setData($agency);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Agency::query()->findOrFail($id);
    }

    public function update(AgencyCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $agency = Agency::query()->findOrFail($id);
        if (!$agency) {
            $this->setMessage('Không có đại lý này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật tên danh mục
                $agency->contact_name                               = $validatedData['contact_name'];
                $agency->slug                                       = $slugify->slugify($validatedData['contact_name']);
                $agency->thumbnails                                 = $validatedData['thumbnails'];
                $agency->position                                   = $validatedData['position'];
                $agency->contact_mobile                             = $validatedData['contact_mobile'];
                $agency->contact_email                              = $validatedData['contact_email'];
                $agency->company                                    = $validatedData['company'];
                $agency->experience                                 = $validatedData['experience'];
                $agency->introduce                                  = $validatedData['introduce'];

                $agency->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($agency);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Agency::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($contact_mobile)
    {
        $agency = Agency::query()->get();
        foreach ($agency->pluck('contact_mobile') as $item) {
            if ($contact_mobile == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $agency = Agency::query()
            ->select([
                'id',
                'contact_name',
                'thumbnails',
                'position',
                'contact_mobile',
                'contact_email',
                'company',
                'experience',
                'introduce',
            ])
            ->where('contact_name', 'LIKE', "%$search%")
            ->orWhere('position', 'LIKE', "%$search%")
            ->orWhere('contact_mobile', 'LIKE', "%$search%")
            ->orWhere('contact_email', 'LIKE', "%$search%")
            ->orWhere('company', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($agency);
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/agency'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}

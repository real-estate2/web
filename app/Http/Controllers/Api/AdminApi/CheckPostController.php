<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\PostCreateRequest;
use App\Post;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\PostStatus;

class CheckPostController extends AbstractApiController
{
    public function index(Request $request)
    {
        $post = Post::query()
            ->select([
                'id',
                'title',
                'slug',
                'price',
                'price_unit',
                'price_total',
                'area',
                'area_unit',
                'address',
                'description',
                'main_direction',
                'nbr_bed_room',
                'nbr_rest_room',
                'nbr_floor',
                'front_street_width',
                'behind_street_width',
                'longitude',
                'latitude',
                'has_image',
                'has_image_360',
                'thumbnails',
                'youtube',
                'contact_name',
                'contact_address',
                'contact_mobile',
                'contact_email',
                'author',
                'approved_by',
                'approved_at',
                'rejected_by',
                'rejected_at',
                'province_id',
                'district_id',
                'ward_id',
                'street_id',
                'project_id',
                'product_type',
                'post_type',
                'product_category',
                'status',
                'start_date',
                'end_date',
            ])
            ->where('status', '=', 0)
            ->DataTablePaginate($request);

        return $this->item($post);
    }

    public function checked($id)
    {
        $post = Post::query()->findOrFail($id);
        if (!$post) {
            $this->setMessage('Không có bài đăng này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $post->approved_by                          = Auth::id();
                $post->approved_at                          = Carbon::now();
                $post->status                               = PostStatus::PUBLISHED;

                $post->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($post);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function refused($id)
    {
        $post = Post::query()->findOrFail($id);
        if (!$post) {
            $this->setMessage('Không có bài đăng này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $post->rejected_by                          = Auth::id();
                $post->rejected_at                          = Carbon::now();
                $post->status                               = PostStatus::REJECTED;

                $post->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($post);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function checkedAll(PostCreateRequest $request, $id, $checker)
    {
        $validatedData = $request->validated();

//        $register_reward = register_reward::query()->findOrFail($id);
//        if (! $register_reward) {
//            $this->setMessage('Không có việc đăng kí thi đua này');
//            $this->setStatusCode(400);
//        } else {
        $payload = [];
        foreach ($request['id'] as $item) {

            $payload['user_id']                         = $item['user_id'];
            $payload['code_number']                     = $item['code_number'];
            $payload['school_id']                       = $item['school_id'];
            $payload['unit_id']                         = $item['unit_id'];
            $payload['rank_staff_id']                   = $item['rank_staff_id'];
            $payload['personal_title_id']               = $item['personal_title_id'];
            $payload['approved_by']                     = $checker;

            // Tạo và lưu đăng kí đã thông qua kiểm duyệt
            $checked_reward = checked_reward::create($payload);
            DB::beginTransaction();
            try {
                $checked_reward->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Kiểm duyệt thành công!');
                $this->setStatusCode(200);
                $this->setData($checked_reward);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Post::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $post = Post::query()
            ->select([
                'id',
                'title',
                'slug',
                'price',
                'price_unit',
                'price_total',
                'area',
                'area_unit',
                'address',
                'description',
                'main_direction',
                'nbr_bed_room',
                'nbr_rest_room',
                'nbr_floor',
                'front_street_width',
                'behind_street_width',
                'longitude',
                'latitude',
                'has_image',
                'has_image_360',
                'thumbnails',
                'youtube',
                'contact_name',
                'contact_address',
                'contact_mobile',
                'contact_email',
                'author',
                'approved_by',
                'approved_at',
                'rejected_by',
                'rejected_at',
                'province_id',
                'district_id',
                'ward_id',
                'street_id',
                'project_id',
                'product_type',
                'post_type',
                'product_category',
                'status',
                'start_date',
                'end_date',
            ])
            ->where('status', '=', 0)
            ->where('title', 'LIKE', "%$search%")
//            ->orWhere('price', 'LIKE', "%$search%")
//            ->orWhere('address', 'LIKE', "%$search%")
//            ->orWhere('area', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($post);
    }
}
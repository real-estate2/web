<?php

namespace App\Http\Controllers;
use App\District;
use Illuminate\Http\Request;

class DistrictController extends AbstractApiController
{
    /**
     * Lấy danh sách quận huyện của tỉnh thành
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDistricts($id)
    {
        $districts = District::query()
            ->select([
                'district_id',
                'pre',
                'name',
            ])
            ->where('province_id', '=', $id)
            ->get();

        return $this->item($districts);
    }

    /**
     * Lấy thông tin tỉnh thành bao gồm cả ranh giới tỉnh
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function geoData($id)
    {
        $district = District::query()
            ->select([
                'latitude',
                'longitude',
                'polygon',
            ])
            ->where('district_id', '=', $id)
            ->firstOrFail();

        return $this->item($district);
    }

    public function bundle($id)
    {
        $query = District::query();
        $query->select([
            'district_id',
            'pre',
            'name',
            'latitude',
            'longitude',
            'polygon',
        ]);
        $query->where('district_id', '=', $id);
        $query->with('wards');
        $query->with('streets');
        $query->with('projects');

        $district = $query->firstOrFail();

        return $this->item($district);
    }
}

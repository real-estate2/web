<?php

namespace App\Http\Controllers;

use App\Province;
use Illuminate\Http\Request;

class ProvinceController extends AbstractApiController
{
    public function index()
    {
        $province = Province::query()
            ->select(['province_id', 'name', 'latitude', 'longitude'])
            ->orderBy('province_id')
            ->get();

        return $this->item($province);
    }

    /**
     * Lấy thông tin tỉnh thành bao gồm cả ranh giới tỉnh
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $province = Province::query()
            ->select([
                'province_id', 'name', 'latitude', 'longitude',
                'polygon',
            ])
            ->where('province_id', '=', $id)
            ->firstOrFail();

        return $this->item($province);
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostCreateRequest;
use App\Post;
use App\PostImage;
use App\Province;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;

class PostController extends Controller
{
    public function getForm()
    {
        $province = Province::query()
            ->select(['province_id', 'name', 'latitude', 'longitude'])
            ->orderBy('province_id')
            ->get();

        return view('customer.post.index', [
            'province' => $province
        ]);
    }

    public function postForm(Request $request)
    {
        dd($request->images);
//        $validatedData = $request->validated();
        $slugify = new Slugify();
        $payload = [];

        $payload['title']                               = $request->title;
        $payload['slug']                                = $slugify->slugify($request->title);

        $payload['price']                               = $request->price;
        $payload['price_unit']                          = $request->price_unit;
        $payload['price_total']                         = 0;

        $payload['area']                                = $request->area;
        $payload['area_unit']                           = $request->area_unit;
        $payload['address']                             = 'Bình dương';
        $payload['description']                         = $request->description;
        $payload['main_direction']                      = $request->main_direction;
        $payload['nbr_bed_room']                        = $request->nbr_bed_room;
        $payload['nbr_rest_room']                       = $request->nbr_rest_room;
        $payload['nbr_floor']                           = $request->nbr_floor;
        $payload['front_street_width']                  = $request->front_street_width;
        $payload['behind_street_width']                 = $request->behind_street_width;


        // Hình ảnh
        $payload['images']                              = ! empty(Session::get('images')) ? Session::get('images') : [];
//        $payload['images']                              = ! empty($request->images) ? $request->images : [];

        // Bản đồ
        $payload['longitude']                           = $request->longitude;
        $payload['latitude']                            = $request->latitude;

        $payload['has_image']                           = false;
        $payload['has_image_360']                       = false;

        $payload['thumbnails']                          = $request->thumbnails;
        $payload['youtube']                             = $request->youtube;
        $payload['contact_name']                        = $request->contact_name;
        $payload['contact_address']                     = $request->contact_address;
        $payload['contact_mobile']                      = $request->contact_mobile;
        $payload['contact_email']                       = $request->contact_email;

        $payload['author']                              = 1;
        $payload['approved_by']                         = 1;
        $payload['approved_at']                         = '2020-02-25';
        $payload['rejected_by']                         = 1;
        $payload['rejected_at']                         = '2020-02-26';

        $payload['province_id']                         = $request->province;
        $payload['district_id']                         = $request->district;
        $payload['ward_id']                             = $request->ward;
        $payload['street_id']                           = $request->street;

        $payload['project_id']                          = $request->project_id;
        $payload['product_type']                        = $request->product_type;
        $payload['post_type']                           = $request->post_type;
        $payload['product_category']                    = $request->product_category;

        $payload['start_date']                          = null;
        $payload['end_date']                            = null;

//        // Kiểm tra trùng tên
//        if (!$this->checkDuplicateName($payload['title'])) {
//            $this->setMessage('Đã tồn tại tiêu đề');
//            $this->setStatusCode(400);
//            return $this->respond();
//        }

        // Tạo và lưu
        $post = Post::create($payload);
        DB::beginTransaction();

        if (! empty($payload['images'])) {
//                foreach ($payload['images'] as $image_idx => $image) {
//                    $currentBucketPath = $image['filepath'];
//
//                    foreach ($image['uploaded_data'] as $thumb_index => $targetFile) {
//                        Storage::disk('minio')->copy(
//                            $currentBucketPath.'/'.$targetFile['filename'],
//                            $newBucketPath.'/'.$targetFile['filename']
//                        );
//
//                        $payload['images'][$image_idx]['filepath'] = $newBucketPath;
//                    }
//                }

            // Cập nhật bucket path

            $post->addImages($payload['images']);
        }

        try {
            $post->save();
            DB::commit();
            // Trả kết quả
//            $this->setMessage('Thêm bài đăng thành công!');
//            $this->setStatusCode(200);
//            $this->setData($post);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
//            $this->setMessage($e->getMessage());
//            $this->setStatusCode(500);
        }
        return back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    public function search(Request $request)
    {
        $request->validate(
            [
                'query' => 'required|min:3',
            ],
            [
                'query.required' => 'Vui lòng nhập ký tự để tìm kiếm!',
                'query.min' => 'Vui lòng nhập ít nhất 3 kí tự!',
            ]
        );

        if ($request->has('query')) {
            $posts = Post::search($request->get('query'))->get();
        } else {
            $posts = Post::query()->get();
        }
//        dd($posts);
        return view('search.index', compact('posts'));

//        $query = $request->input('query');
//        $rent = Post::where('title','like', "%$query%")
//            ->where('status', '!=', 0)
//            ->where('project_id', '===', 0)
//            ->where('product_type', '=', 49)
//            ->get();
//
//        $sell = Post::where('title','like', "%$query%")
//            ->where('status', '!=', 0)
//            ->where('project_id', '===', 0)
//            ->where('product_type', '=', 38)
//            ->get();
//
//        $project = Post::where('title','like', "%$query%")
//            ->where('status', '!=', 0)
//            ->where('project_id', '!==', 0)
//            ->get();
//
//        $count_list = count($rent) + count($sell) + count($project);

//        return view('search.index', ['project' => $project, 'sell' => $sell, 'rent' => $rent, 'countList' => $count_list]);
    }
}

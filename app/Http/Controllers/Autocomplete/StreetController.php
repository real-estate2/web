<?php
namespace App\Http\Controllers\Autocomplete;

use App\Http\Controllers\Controller;
use App\Street;
use Illuminate\Http\Request;

class StreetController extends Controller
{
    /**
     * Lấy danh sách quận huyện của tỉnh thành
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStreets($id)
    {
        $streets = Street::query()
            ->select([
                'district_id',
                'street_id',
                'name',
            ])
            ->where('district_id', '=', $id)
            ->get();

        return response()->json($streets);
    }
}

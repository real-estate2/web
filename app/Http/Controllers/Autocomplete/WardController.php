<?php

namespace App\Http\Controllers\Autocomplete;

use App\Http\Controllers\Controller;
use App\District;
use App\Ward;
use Illuminate\Http\Request;

class WardController extends Controller
{
    /**
     * Lấy danh sách quận huyện của tỉnh thành
     *
     * @param $id
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getWards($id)
    {
        $wards = Ward::query()
            ->select([
                'district_id',
                'ward_id',
                'name',
            ])
            ->where('district_id', '=', $id)
            ->get();

        return response()->json($wards);
    }
}

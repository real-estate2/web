<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function getLogin()
    {
        return view('login.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request,
            [
                'username' => 'required',
                'password' => 'required|min:6|max:20',
            ],
            [
                'username.required'         => 'Vui lòng nhập tài khoản!',
                'password.required'         => 'Vui lòng nhập mật khẩu',
                'password.min'              => 'Mật khẩu ít nhất 6 kí tự',
                'password.max'              => 'Mật khẩu không quá 20 kí tự',
            ]
        );

        $credentials = array('username' => $request->username, 'password'=>$request->password, 'post_privileged' => false);
        if(Auth::attempt($credentials))
        {
//            $request->session()->put('user', Auth::user());
//            dd($request->session()->get('user')->username);
//            dd(Session::get('user')->username);

            return redirect()->route('home.get')->with(['flag'=>'success','login-notification'=>'Đăng nhập thành công!']);
        }
        else{
            return redirect()->back()->with(['flag'=>'danger','login-notification'=>'Đăng nhập thất bại! Vui lòng kiểm tra lại tài khoản hoặc mật khẩu?']);
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home.get');
    }

    public function getRegister()
    {
        return view('login.register');
    }

    public function postRegister(Request $request)
    {
        $this->validate($request,
            [
                'email'                      => 'required|email|unique:users,email',
                'password'                      => 'required| min:6 | max:20',
                're_password'                   => 'required|same:password',
                'username'                         => 'required',
//                'phone' => 'required',
                'mobile'                        => 'required',
                'last_name'                     => 'required',
                'first_name'                    => 'required',
                'sex'                           => 'required',
//                'captcha' => 'required|captcha',
            ],
            [
                'email.required'                => 'Vui lòng nhập email',
                'enail.email'                   => 'Không đúng định dạng email',
                'email.unique'                  => 'Email đã có người sử dụng',

                'password.required'             => 'Vui lòng nhập password',
                'password.min'                  => 'Mật khẩu ít nhất 6 kí tự',

                're_password.required'          => 'Mật khẩu nhập lại không để trống',
                're_password.same'              => 'Mật khẩu không giống nhau',

                'username.required'             => 'Vui lòng nhập tài khoản',
                'mobile.required'               => 'Vui lòng nhập di động',
                'last_name.required'            => 'Vui lòng nhập họ',
                'first_name.required'           => 'Vui lòng nhập tên',
                'sex.required'                  => 'Vui lòng nhập giới tính',

//                'captcha.required' => 'Vui lòng nhập captcha',
//                'captcha.captcha' => 'Vui lòng nhập đúng captcha',
            ]);
        $user = new User();
        $user->username                         = $request->username;
        $user->password                         = Hash::make($request->password);
        $user->email                            = $request->email;
        $user->email_verified_at                = Carbon::now();
        $user->phone                            = $request->phone;
        $user->mobile                           = $request->mobile;
        $user->mobile_verified_at               = Carbon::now();
        $user->last_name                        = $request->last_name;
        $user->first_name                       = $request->first_name;
        $user->sex                              = $request->sex;

        // Quyền hạn
        $user->post_privileged                  = false;
        $user->post_privileged_granted_at       = Carbon::now();

        $user->save();

        return redirect()->back()->with('register-success', 'Chúc mừng bạn đã tạo tài khoản thành công!');
    }

    public function refresh()
    {
        return captcha_img();
    }
}

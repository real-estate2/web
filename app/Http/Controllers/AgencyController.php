<?php

namespace App\Http\Controllers;

use App\Agency;

class AgencyController extends Controller
{
    public function index()
    {
        $agency = Agency::all();
        return view('agency.index', [
            'agency' => $agency
        ]);
    }

    public function detail($slug, $id)
    {
        $agency = Agency::query()->findOrFail($id);

        return view('agency.detail', [
            'item' => $agency,
        ]);
    }
}

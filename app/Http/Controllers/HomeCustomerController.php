<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class HomeCustomerController extends Controller
{
    public function index()
    {
        // Căn hộ
        $apartment = Post::query() // mặc định là bán
        // ->where('project_id', '!=', 0)
        ->where('status', '!=', 0)
            ->where('product_category', '=', 324)
            ->get();

        $apartment_rent = Post::query()
            // ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->where('product_category', '=', 326)
            ->get();

        $townhouse_project = Post::query()
            // ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->get();

        $land_project = Post::query()
            // ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->where('product_category', '=', 40)
            ->get();

        $home_private = Post::query()
            // ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->where('product_category', '=', 52)
            ->get();

//        $rent = Post::query()
//            ->where('project_id', '!=', 0)
//            ->where('status', '!=', 0)
//            ->where('product_type', '=', 49)
//            ->get();
//
//        $sell = Post::query()
//            ->where('project_id', '!=', 0)
//            ->where('status', '!=', 0)
//            ->where('product_type', '=', 38)
//            ->get();

        $resort = Post::query()
            // ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->where('product_category', '=', 44)
            ->get();

        $storehouse = Post::query()
            // ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->where('product_category', '=', 45)
            ->get();

        $storehouse_rent = Post::query()
            // ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->where('product_category', '=', 53)
            ->get();

        $land = Post::query()
            // ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->where('product_category', '=', 283)
            ->get();

        $home_street = Post::query()
            // ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->where('product_category', '=', 163)
            ->get();

        $office = Post::query()
            // ->where('project_id', '!=', 0)
            ->where('status', '!=', 0)
            ->where('product_category', '=', 50)
            ->get();



        return view('home.index', [
            'apartment' => $apartment,
            'apartment_rent' => $apartment_rent,
            'townhouse_project' => $townhouse_project,
            'land_project' => $land_project,
            'land' => $land,
            'home_street'=>$home_street,
//            'rent' => $rent,
            'storehouse' => $storehouse,
            'storehouse_rent' => $storehouse_rent,
//            'sell' => $sell,
            'resort' => $resort,
            'home_private'=>$home_private,
            'office'=>$office,
        ]);
    }

    public function image()
    {
        return view('image');
    }
}

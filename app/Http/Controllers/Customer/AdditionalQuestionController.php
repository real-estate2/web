<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;

use App\AdditionalQuestion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdditionalQuestionController extends Controller
{
    public function postForm(Request $request, $id)
    {
        $payload = [];

        $payload['post_id']                                 = $id;

        $payload['contact_name']                            = $request->contact_name;
        $payload['contact_address']                         = '';
        $payload['contact_mobile']                          = $request->contact_mobile;
        $payload['contact_email']                           = $request->contact_email;
        $payload['sex']                                     = $request->sex;
        $payload['question']                                = $request->question;

//        // Kiểm tra trùng tên
//        if (!$this->checkDuplicateName($payload['title'])) {
//            $this->setMessage('Đã tồn tại tiêu đề');
//            $this->setStatusCode(400);
//            return $this->respond();
//        }

        // Tạo và lưu
        $additionalQuestion = AdditionalQuestion::create($payload);
        DB::beginTransaction();


        try {
            $additionalQuestion->save();
            DB::commit();
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
        }

        return redirect()->back()->with(['customer'=>'success','customer-notification'=>'Gửi yêu cầu thành công. Chúng tôi sẽ sớm liên hệ. Cảm ơn bạn!']);
    }
}

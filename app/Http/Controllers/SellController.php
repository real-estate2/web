<?php

namespace App\Http\Controllers;

use App\OutstandingLocation;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use McCool\LaravelAutoPresenter\Facades\AutoPresenter;

class SellController extends Controller
{
    public function index()
    {
        $post = Post::query()
            ->with('users')
            ->where('product_type', '=', 38)
            ->where('status', '!=', 0)
            ->orderBy('post_type')
            ->orderByDesc('created_at')
            ->get(); // 49: loại cho thuê || 38: loại bán

//        dd($post);

//        return response()->json($post);

        $arrUser = [];

        foreach ($post as $item) {
            $user = User::query()->where('id', '=', $item->author)->get();
            array_push($arrUser, [
                'filename' => $user
            ]);
        }

        return view('post.sell.index', [
            'post' => $post,
            'user' => $arrUser
        ]);
    }

    public function details($slug, $id)
    {
        $post = Post::query()->findOrFail($id);

        $post_order = Post::query()
            ->where('product_type', '=', 38)
            ->where('status', '!=', 0)
            ->orderBy('post_type')
            ->take(4)
            ->get(); // 49: loại cho thuê || 38: loại bán

        $post->load('images');

        $list_image = [];
        $list_image_360 = [];

        foreach ($post->images as $item) {
            if ($item->type == 'image_360') {
                $list_image_360[] = $item;
            } else {
                $list_image[] = $item;
            }
        }

        $query = OutstandingLocation::query();
        $query->distance($post->latitude, $post->longitude);
        $query->orderBy('distance', 'ASC')->get();
        $query->limit(5);

        return view('post.sell.detail', [
            'item' => $post,
            'list_image' => $list_image,
            'order' => $post_order,

            'data' => AutoPresenter::decorate($post),
            'nearBy' => $query->get(),
        ]);
    }
}

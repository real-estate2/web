<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, DataTablePaginate, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'last_name',
        'first_name',
        'email',
        'email_verified_at',
        'mobile',
        'mobile_verified_at',
        'sex',
        'post_privileged',
        'post_privileged_granted_at'
    ];

    protected $filter = [
        'id',
        'username',
        'password',
        'last_name',
        'first_name',
        'email',
        'email_verified_at',
        'mobile',
        'mobile_verified_at',
        'sex',
        'post_privileged',
        'post_privileged_granted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}

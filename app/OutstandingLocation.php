<?php

namespace App;

use App\Presenters\OutstandingLocationPresenter;
use Illuminate\Database\Eloquent\Model;
use Malhal\Geographical\Geographical;
use McCool\LaravelAutoPresenter\HasPresenter;

/**
 * Class OutstandingLocation
 *
 * @property mixed name
 * @property mixed type
 * @property mixed geometry
 * @property mixed longitude
 * @property mixed latitude
 * @property mixed zoom_level
 */
class OutstandingLocation extends Model implements HasPresenter
{
    use Geographical;

    protected static $kilometers = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'remote_id',
        'name',
        'type',
        'geometry',
        'longitude',
        'latitude',
        'zoom_level',
    ];

    /**
     * Get the presenter class.
     *
     * @return string
     */
    public function getPresenterClass()
    {
        return OutstandingLocationPresenter::class;
    }

    /**
     * Create outstanding location from parse object
     *
     * @param mixed $input
     */
    public function setOutstandingLocation($input)
    {
        $this->objectId = $input->getObjectId();
        $this->name = $input->get('name');
        $this->type = $input->get('type');
        $this->longitude = $input->get('longitude');
        $this->latitude = $input->get('latitude');
        $this->zoom_level = $input->get('zoom_level');
    }
}

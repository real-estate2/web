<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostImage extends Model
{
    protected $fillable = [
        'post_id',
        'filepath',
        'type',
        'o',
        'xs',
        'uploaded_data',
        'sort_order',
    ];

    protected $casts = [
        'uploaded_data' => 'array',
    ];
}

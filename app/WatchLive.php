<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class WatchLive extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'post_id',
        'contact_name',
        'contact_address',
        'contact_mobile',
        'contact_email',
        'sex',
        'live_date',
    ];

    protected $filter = [
        'id',
        'post_id',
        'contact_name',
        'contact_address',
        'contact_mobile',
        'contact_email',
        'sex',
        'live_date',
    ];

    public function posts()
    {
        return $this->belongsTo(Post::class, 'post_id','id');
    }
}

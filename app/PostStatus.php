<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostStatus extends Model
{
    public const PENDING = 0;
    public const PUBLISHED = 1;
    public const REJECTED = 2;
    public const NEED_UPDATE = 3;
    public const PENDING_REMOVE = 4;
}

<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;
use McCool\LaravelAutoPresenter\HasPresenter;
use App\Presenters\PostPresenter;
use Laravel\Scout\Searchable;

class Post extends Model implements HasPresenter
{
    use Searchable;
    use DataTablePaginate;


    protected $table = 'posts';

    protected $fillable = [
        'title',
        'slug',
        'price',
        'price_unit',
        'price_total',
        'area',
        'area_unit',
        'address',
        'description',
        'main_direction',
        'nbr_bed_room',
        'nbr_rest_room',
        'nbr_floor',
        'front_street_width',
        'behind_street_width',
        'longitude',
        'latitude',
        'has_image',
        'has_image_360',
        'thumbnails',
        'youtube',
        'contact_name',
        'contact_address',
        'contact_mobile',
        'contact_email',
        'author',
        'approved_by',
        'approved_at',
        'rejected_by',
        'rejected_at',
        'province_id',
        'district_id',
        'ward_id',
        'street_id',
        'project_id',
        'product_type',
        'post_type',
        'product_category',
        'status',
        'start_date',
        'end_date',
        'pending_remove',
    ];

    protected $filter = [
        'id',
        'title',
        'slug',
        'price',
        'price_unit',
        'price_total',
        'area',
        'area_unit',
        'address',
        'description',
        'main_direction',
        'nbr_bed_room',
        'nbr_rest_room',
        'nbr_floor',
        'front_street_width',
        'behind_street_width',
        'longitude',
        'latitude',
        'has_image',
        'has_image_360',
        'thumbnails',
        'youtube',
        'contact_name',
        'contact_address',
        'contact_mobile',
        'contact_email',
        'author',
        'approved_by',
        'approved_at',
        'rejected_by',
        'rejected_at',
        'province_id',
        'district_id',
        'ward_id',
        'street_id',
        'project_id',
        'product_type',
        'post_type',
        'product_category',
        'status',
        'start_date',
        'end_date',
        'pending_remove',
    ];

    // Test RemoveImagesOld
    public function removeImages($imagesOld)
    {
        $query = PostImage::query();
        foreach ($imagesOld as $idImages) {
            $query->where('post_id', '=', $this->id)
                ->where('id', '!=', $idImages);
        }
        $query->delete();
    }

    /**
     * Thêm hình ảnh vào bài đăng
     *
     * @param  mixed  $images
     */

    public function addImages($images)
    {
        $has_image     = false;
        $has_image_360 = false;

        foreach ($images as $imageObj) {
            $d = [
                'post_id' => $this->id,

                'filepath'      => $imageObj['filepath'],
                'type'          => $imageObj['type'],
                'o'             => null,
                'xs'            => null,
                'uploaded_data' => $imageObj['uploaded_data'],
                'sort_order'    => $imageObj['sort_order'],
            ];

            foreach ($imageObj['uploaded_data'] as $image) {
                if ($image['ranting'] == 'o') {
//                    $d['o'] = $imageObj['filepath'].'/'.$image['filename'];
                    $d['o'] = $image['filename'];
                }

                if ($image['ranting'] == 'xs') {
//                    $d['xs'] = $imageObj['filepath'].'/'.$image['filename'];
                    $d['xs'] = $image['filename'];
                }
            }

            if ($imageObj['type'] == 'image') {
                $has_image = true;
            } elseif ($imageObj['type'] == 'image_360') {
                $has_image_360 = true;
            }

            PostImage::create($d);
        }

        $this->has_image     = $has_image;
        $this->has_image_360 = $has_image_360;
    }

    /**
     * Thay thế ảnh của bài đăng
     *
     * @param  mixed  $images
     *
     * @throws \Exception
     */
    public function setImages($images)
    {
        PostImage::where('post_id', '=', $this->id)->delete();

        $this->addImages($images);
    }

    public function images()
    {
        return $this->hasMany(PostImage::class, 'post_id', 'id');
    }

    public function districts()
    {
        return $this->belongsTo(district::class, 'district_id', 'district_id');
    }

    public function projects()
    {
        return $this->belongsTo(Category::class, 'project_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'author', 'id');
    }

    public function getPresenterClass()
    {
        return PostPresenter::class;
    }

    // Search scout ====================================================================
    public function searchableAs()
    {
        return 'posts_index';
    }
}

<?php

namespace App\Models\AC;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{
    protected $fillable = [
        'district_id',
        'ward_id',
        //'pre',
        'name',
        'full_address',
    ];

    protected $visible = [
        'ward_id',
        'name',
        'full_address',
    ];
}

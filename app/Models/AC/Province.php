<?php

namespace App\Models\AC;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = [
        'city_id',
        'name',

        'polygon',
        'longitude',
        'latitude',
    ];

    protected $spatialFields = [
        //'polygon'
    ];
}

<?php

namespace App\Models\AC;

use Illuminate\Database\Eloquent\Model;

class Street extends Model
{
    protected $fillable = [
        'district_id',
        'street_id',
        //'pre',
        'name',
    ];

    protected $visible = [
        'street_id',
        'name',
    ];
}

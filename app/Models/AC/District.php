<?php

namespace App\Models\AC;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = [
        'city_id',
        'district_id',
        //'pre',
        'name',
        'full_address',
        'keywords',

        'polygon',
        'longitude',
        'latitude',
    ];


    public function wards()
    {
        return $this->hasMany(Ward::class, 'district_id', 'district_id');
    }

    public function streets()
    {
        return $this->hasMany(Street::class, 'district_id', 'district_id');
    }
}

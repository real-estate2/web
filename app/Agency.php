<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'contact_name',
        'slug',
        'thumbnails',
        'position',
        'contact_mobile',
        'contact_email',
        'company',
        'experience',
        'introduce',
    ];

    protected $filter = [
        'id',
        'contact_name',
        'slug',
        'thumbnails',
        'position',
        'contact_mobile',
        'contact_email',
        'company',
        'experience',
        'introduce',
    ];
}

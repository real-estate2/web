<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\AC\Ward;
use App\Models\AC\Street;
class District extends Model
{
    protected $fillable = [
        'city_id',
        'district_id',
        //'pre',
        'name',
        'full_address',
        'keywords',

        'polygon',
        'longitude',
        'latitude',
    ];


    public function wards()
    {
        return $this->hasMany(\App\Ward::class, 'district_id', 'district_id');
    }

    public function streets()
    {
        return $this->hasMany(\App\Street::class, 'district_id', 'district_id');
    }

    public function projects()
    {
        return $this->hasMany(Category::class, 'district_id', 'district_id');
    }
}

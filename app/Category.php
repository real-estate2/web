<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'district_id',
        'name',
        'description',
        'thumbnails',
    ];

    protected $filter = [
        'id',
        'district_id',
        'name',
        'description',
        'thumbnails',
    ];
}

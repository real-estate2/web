<?php


namespace App\Presenters;

use App\Presenters\Traits\TimestampsTrait;
use Illuminate\Contracts\Support\Arrayable;
use McCool\LaravelAutoPresenter\BasePresenter;

class OutstandingLocationPresenter extends BasePresenter implements Arrayable
{
    use TimestampsTrait;

    public function getDistanceString()
    {
        $distance = $this->wrappedObject->distance;

        if ($distance < 1) {
            return round($distance*1000) . ' m';
        }

        return round($distance, 1) . ' km';
    }

    /**
     * Convert the presenter instance to an array.
     *
     * @return string[]
     */
    public function toArray()
    {
        return array_merge($this->wrappedObject->toArray(), [
            'created_at' => $this->created_at(),
            'updated_at' => $this->updated_at(),
        ]);
    }
}
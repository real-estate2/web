<?php


namespace App\Presenters;

use App\Presenters\Traits\TimestampsTrait;
use Illuminate\Contracts\Support\Arrayable;
use McCool\LaravelAutoPresenter\BasePresenter;

class PostPresenter extends BasePresenter implements Arrayable
{
    use TimestampsTrait;

    public function getPriceUnitString($d, $t)
    {
        if (! empty($d)) {
            if ($t === 49) {
                $unit = [
                    0 => 'Thỏa thuận',
                    1 => 'Trăm nghìn/tháng',
                    2 => 'Triệu/tháng',
                    5 => 'Trăm nghìn/m2/tháng',
                    6 => 'Triệu/m2/tháng',
                    7 => 'Nghìn/m2/tháng',
                ];
            } else {
                $unit = [
                    0 => 'Thỏa thuận',
                    1 => 'Triệu',
                    2 => 'Tỷ',
                    6 => 'Trăm nghìn/m2',
                    7 => 'Triệu/m2',
                ];
            }
            return $unit[$d];
        }

        return 'Không xác định';
    }

    public function getProductPriceUnitString()
    {
        return $this->getPriceUnitString($this->wrappedObject->price_unit, $this->wrappedObject->product_type);
    }

    public function getTypeString($d)
    {
        if (! empty($d)) {
            $type = [
                38 => 'Bán',
                49 => 'Cho thuê',
            ];

            return $type[$d];
        }

        return 'Không xác định';
    }

    public function getProductTypeString()
    {
        return $this->getTypeString($this->wrappedObject->product_type);
    }

    public function getCategoryString($d)
    {
        if (! empty($d)) {
            $category = [
                // Bán
                324     =>      'Bán căn hộ chung cư',
                41      =>      'Bán nhà riêng',
                325     =>      'Bán nhà biệt thự, liền kề',
                163     =>      'Bán nhà mặt phố',
                40      =>      'Bán đất nền dự án',
                283     =>      'Bán đất',
                44      =>      'Bán trang trại, khu nghỉ dưỡng',
                45      =>      'Bán kho, nhà xưởng',
                48      =>      'Bán loại bất động sản khác',
                // Mua
                326     =>      'Cho thuê căn hộ chung cư',
                52      =>      'Cho thuê nhà riêng',
                51      =>      'Cho thuê nhà mặt phố',
                57      =>      'Cho thuê nhà trọ, phòng trọ',
                50      =>      'Cho thuê văn phòng',
                55      =>      'Cho thuê cửa hàng, ki ốt',
                53      =>      'Cho thuê kho, nhà xưởng, đất',
                59      =>      'Cho thuê loại bất động sản khác',
            ];

            return $category[$d];
        }

        return 'Không xác định';
    }

    public function getProductCategoryString()
    {
        return $this->getCategoryString($this->wrappedObject->product_category);
    }

   public function getPostString($d)
   {
       if (! empty($d)) {
           $post = [
               1 => 'Tin đặc biệt',
               2 => 'Tin thường',
           ];

           return $post[$d];
       }

       return 'Không xác định';
   }

   public function getPostTypeString()
   {
       return $this->getPostString($this->wrappedObject->post_type);
   }

    /**
     * Lấy tên hướng từ mã số
     *
     * Truyền vào mã hướng và trả về tên của hướng đó
     *
     * @param int $d
     *
     * @return mixed|string
     */
    public function getDirectionString($d)
    {
        if (! empty($d)) {
            $direction = [
                0 => 'KXĐ',
                1 => 'Chánh Đông',
                2 => 'Chánh Tây',
                3 => 'Chánh Nam',
                4 => 'Chánh Bắc',
                5 => 'Đông-Bắc',
                6 => 'Tây-Bắc',
                7 => 'Tây-Nam',
                8 => 'Đông-Nam',
            ];

            return $direction[$d];
        }

        return 'Không xác định';
    }

    /**
     * Chuyển mã hướng thành chữ
     *
     * @return mixed|string
     */
    public function getMainDirectionString()
    {
        return $this->getDirectionString($this->wrappedObject->main_direction);
    }

    /**
     * Convert the presenter instance to an array.
     *
     * @return string[]
     */
    public function toArray()
    {
        return array_merge($this->wrappedObject->toArray(), [
            'main_direction_string'         => $this->getMainDirectionString(),
//            'post_type_string'             => $this->getPostTypeString(),
            'price_unit_string'             => $this->getProductPriceUnitString(),
            'product_category_string'             => $this->getProductCategoryString(),
            'product_type_string'             => $this->getProductTypeString(),
            'pending_delete'                => $this->wrappedObject->pending_delete ? $this->utc_to_hcm($this->wrappedObject->pending_delete) : null,
            'created_at'                    => $this->created_at(),
            'updated_at'                    => $this->updated_at(),
        ]);
    }
}
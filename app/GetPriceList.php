<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class GetPriceList extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'post_id',
        'contact_name',
        'contact_address',
        'contact_mobile',
        'contact_email',
        'sex',
    ];

    protected $filter = [
        'id',
        'post_id',
        'contact_name',
        'contact_address',
        'contact_mobile',
        'contact_email',
        'sex',
    ];

    public function posts()
    {
        return $this->belongsTo(Post::class, 'post_id','id');
    }
}
